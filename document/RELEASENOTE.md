# Release notes


#### 1.0.0

* Key features.

   * Multiple presentation support.
   * Support for images (png,jpeg,gif).
   * Support for videos (mp4,avi).
   * Support for webUrl (http, https).
   * Support for static Web pages (file).
   * Support for scroll and static text (txt).
   * Support for rss.
   * Support for clock.

* Content sync on presentation demand.
* Content sanity check using md5 on fixed interval.
* Removing unused content to clear space.