# Developer Guide

Playing signage on any application in less than few minutes.

DsCore is signage library which manages content downloading,
and play schedules as per the rules. Regions rendering .

### Usage

#### Initializing


**Programmatically**

```

SignageViewFragment fragment =  new SignageViewFragment();

getSupportFragmentManager()
            .beingTransaction()
            .replace(R.id.your_frame_id,fragment)
            .commit();

```

**In layout**

```
   In layout file

    <fragment
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:id="@+id/mSignageFragment"
        android:tag="prosignsdk.SignageViewFragment"
        android:name="prosignsdk.SignageViewFragment"
        />


```

 then in Activity

```
  SignageViewFragment signageViewFragment = getSupportFragmentManager()
                                                    .findFragmentById(R.id.mSignageFragment);
```


### Running Signage

Running signage requires explicit call to start.

// Fixed it not working
```
 signageViewFragment.startSignage(YourApiEndPoint);
```

EndPoint Api canbe http or https or file.

### Stopping Signage

SignageViewFramgent automatically stops when remove.

No explicit call requires

### Listening Signage events*

To listen signage events application needs to implement SyncStatusListener
in there application

```
 signageViewFragment.setSyncStatusListener(new SyncStatusListener() {
                        @Override
                        public void syncStatus(RsyncEvent event) {
                                // TODO: stuff
                        }
                });

```

<br/>

**RsyncEvent**

1. SYNC_STARTED,
2. SYNC_COMPLETE,
3. SYNC_FAILED

<br/>
<br/>


\* Listener subjected to change
