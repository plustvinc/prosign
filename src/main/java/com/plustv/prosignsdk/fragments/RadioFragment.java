package com.plustv.prosignsdk.fragments;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;

import java.io.File;
import java.io.IOException;

import static com.plustv.prosignsdk.common.FragmentPoolManager.RADIO;

public class RadioFragment extends BaseFragment  {

FrameLayout parentView;
MediaPlayer mPlayer;
ImageView imgAudio;


@Override
public int getLayout() {
	  return R.layout.fragment_radio;
}


@Override
public void onCreateView(View view) {
	  parentView = (FrameLayout) view;
	  imgAudio = view.findViewById(R.id.imgAudio);

	  mPlayer = new MediaPlayer();

	  if ( this.asset.type == RADIO) {

		    Glide.with(this)
				.load(R.drawable.radio)
				.into(imgAudio);

		    //Set the audio stream type of the media player:
		    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);



		    //Set the data source as a content Uri:
		    try {
				this.mPlayer.setDataSource(getActivity(), Uri.parse(this.asset.src));
		    } catch (IllegalArgumentException |SecurityException |IOException  e) {
		    		e.printStackTrace();
		    }




		    //Prepare the player for playback:
		    try {

			    this.mPlayer.prepareAsync();
			    this.mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					  mp.start();
				}
		    });


		    } catch (Exception e) {
				e.printStackTrace();

		    }


	  }
	  else{

		    if (FileUtility.fileExists(this.asset.name)) {
				File file = FileUtility.getFile(this.asset.name);
				try {
					  this.mPlayer.setDataSource(file.getAbsolutePath());
				} catch (IOException e) {
					  e.printStackTrace();
				}
		    } else {
				Glide.with(this)
					  .load(R.drawable.audio_thumbnail)
					  .into(imgAudio);
				Log.e(" check live url "," check live url ");

		    }

	  }

}


@Override
public void onDetach() {
	  imgAudio = null;
	  if(mPlayer!=null) {
		    mPlayer.stop();
		    mPlayer.release();
		    mPlayer = null;
	  }
	  super.onDetach();
}




}


