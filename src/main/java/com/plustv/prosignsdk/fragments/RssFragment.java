package com.plustv.prosignsdk.fragments;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.ScrollingTextView;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.RssParser;
import com.plustv.prosignsdk.models.Properties;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;



public class RssFragment extends BaseFragment {

    //private static TextView scrollingTextView;
    static ScrollingTextView scrollingTextView;
    long duration;
    private Properties spanProperties;
    private Handler loopHandler = new Handler();
    long intervalDuration = 0;
    @Override
    public int getLayout() {
        return R.layout.scroll_fragment;
    }

    @Override
    public void onCreateView(final View view) {
        scrollingTextView = view.findViewById(R.id.scrollText);
        duration = (spanProperties.rate)*1000;  //sec


        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                /*Top and bottom padding*/
                //int fontSize = ((int) (scrollingTextView.getMeasuredHeight()/getScreenDensity())) - 6;
                //scrollingTextView.setTextSize(fontSize);
            }
        });

        spanProperties = this.asset.assetProperty;
        //scrollingTextView.setTextSize(20);
        scrollingTextView.setTextSize(spanProperties.fontSize);
        scrollingTextView.setTextColor(parseColor(spanProperties.color));
        scrollingTextView.setBackgroundColor(parseColor(spanProperties.bg));
        scrollingTextView.setGravity(Gravity.CENTER_VERTICAL);
        scrollingTextView.setRndDuration(duration);
        scrollingTextView.startScroll();
//        scrollingTextView.setSelected(true);

        if("rtl".equalsIgnoreCase(spanProperties.direction)) {
            scrollingTextView.setTextDirection(View.TEXT_DIRECTION_RTL);
        }

        intervalDuration = spanProperties.refresh*1000;

        if(intervalDuration>=10000) {
            // For get xml data instant
            loopHandler.post(loopRunnable);
        } else {
            // Get xml at least once.
            new AsyncTaskThread().execute(asset.src);
        }

    }

    Runnable loopRunnable = new Runnable() {
        @Override
        public void run() {
                loopHandler.removeCallbacks(loopRunnable);
                new AsyncTaskThread().execute(asset.src);
                loopHandler.postDelayed(loopRunnable,intervalDuration);
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        ((ViewGroup)getView().getParent()).setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        scrollingTextView = null;
        loopHandler.removeCallbacks(loopRunnable);
    }

    static class AsyncTaskThread extends AsyncTask<String,String,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            URL url = null;
            String result="";
            try {
                url = new URL(params[0]);
                InputStream inputStream = url.openConnection().getInputStream();
                result= RssParser.rssFeed(inputStream);
            } catch (IOException | XmlPullParserException e) {
                e.printStackTrace();
                return "";
            }

            return result;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            scrollingTextView.setText(s);
        }

    }



}

