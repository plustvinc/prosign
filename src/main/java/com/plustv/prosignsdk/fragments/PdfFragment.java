package com.plustv.prosignsdk.fragments;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.github.barteksc.pdfviewer.PDFView;

import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;

import java.io.File;

import static com.plustv.prosignsdk.common.FileUtility.getFile;


/**
 * Created by Rachit Solanki on 19/8/17.
 */

public class PdfFragment extends BaseFragment {
    PDFView pdfView;
    LinearLayout lPdfLayout;

    @Override
    public int getLayout() {
        return R.layout.fragment_pdf;
    }

    @Override
    public void onCreateView(View view) {
        lPdfLayout = view.findViewById(R.id.lPdfLayout);
        pdfView = view.findViewById(R.id.pdfView);

        if( FileUtility.fileExists(asset.name) &&
                !FileUtility.isLockExists(asset.name)) {
            File f = getFile(asset.name);
            File url = f.getAbsoluteFile();
            Log.e(" pdf"," pdf "+ url);
            pdfView.fromFile(url).load();

        } else {
            pdfView.fromAsset("plus.pdf").load();

        }



    }


}







