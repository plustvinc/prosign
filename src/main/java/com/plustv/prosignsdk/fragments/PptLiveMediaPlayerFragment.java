package com.plustv.prosignsdk.fragments;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;

import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;

import java.io.File;
import java.io.IOException;

import static com.plustv.prosignsdk.common.FragmentPoolManager.LIVE_STREAMING;


public class PptLiveMediaPlayerFragment extends BaseFragment implements Callback {
    public MediaPlayer mediaPlayer;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;

    @Override
    public int getLayout() {
        return R.layout.surface_view_fragment;
    }

    FrameLayout parentView;
    @Override
    public void onCreateView(View view) {
        parentView = (FrameLayout) view;
        this.surfaceView = view.findViewById(R.id.surfaceView);
        this.surfaceHolder = this.surfaceView.getHolder();
        this.surfaceHolder.addCallback(this);
     }

    public void setMediaPlayer() {
        try {
            this.mediaPlayer = new MediaPlayer();
            this.mediaPlayer.setLooping(false);

            /*
            * Check if asset type is live streaming
            *  Supported RTSP, UDP, HLS, HTTP, HTTPS.
            */
            if ( this.asset.type == LIVE_STREAMING) {
                this.mediaPlayer.setDataSource(getActivity(),Uri.parse(this.asset.src));
            }else {
                if (FileUtility.fileExists(this.asset.name+".mp4")) {
                    File file = FileUtility.getFile(this.asset.name+".mp4");
                    this.mediaPlayer.setDataSource(file.getAbsolutePath());
                } else {
                    Uri uri = Uri.parse( "android.resource://"+getActivity().getPackageName()+"/" +R.raw.default_vid);
                    this.mediaPlayer.setDataSource(getActivity(),uri);
                }

            }
           this.mediaPlayer.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT);

        } catch (IOException e){
            Log.e("MediaPlayer","IOException while setting");
        }


    }

    @Override
    public void onDetach() {
        if(mediaPlayer!=null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                parentView.removeAllViews();
            }
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer=null;
        }
        super.onDetach();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        try {

            setMediaPlayer();
            this.mediaPlayer.setDisplay(this.surfaceHolder);
            this.mediaPlayer.prepareAsync();
            this.mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.e("Surface Destroyed","++++++======+++++++++===============");
    }
}