package com.plustv.prosignsdk.fragments;


import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterViewFlipper;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.adapter.DirectoryAdapter;
import com.plustv.prosignsdk.common.BaseFragment;

import com.plustv.prosignsdk.models.CustomDoctorList;
import com.plustv.prosignsdk.models.DoctorDirectory;
import com.plustv.prosignsdk.models.Properties;

import java.util.ArrayList;
import java.util.List;

public class DirectoryFragment extends BaseFragment {

    public DoctorDirectory doctorDirectories;
    private List<CustomDoctorList> customList = new ArrayList<>();
    public Properties properties;
    AdapterViewFlipper adapterViewFlipper;
    LinearLayout lDataFragmentLayout;
    LinearLayout lScrollLayout;
    TextView tvScrollText;


    @Override
    public int getLayout() {
        return R.layout.fragment_directory;
    }


    @Override
    public void onCreateView(View view) {

        adapterViewFlipper = view.findViewById(R.id.adapterViewFlipper);
        lDataFragmentLayout = view.findViewById(R.id.lDataFragmentLayout);
        lScrollLayout = view.findViewById(R.id.lScrollLayout);
        tvScrollText = view.findViewById(R.id.tvScrollText);


            doctorDirectories = this.asset.doctorDirectories;
            if (doctorDirectories != null) {

            properties = this.asset.assetProperty;

            Typeface typefaceBold = Typeface.createFromAsset(this.getContext().getAssets(), "font/gotham_rounded_bold_21016.ttf");

            String bg = properties.bg;
            String scrollBackgroundColor = properties.departmentHeaderColor;
            String scrollTextColor = properties.departmentNameColor;  // departmentNameColor

            int templateBackgroundColorCode = Color.parseColor(bg);
            int departmentHeaderBackgroundColorCode = Color.parseColor(scrollBackgroundColor);
            int scrollTextColorCode = Color.parseColor(scrollTextColor);
            int scrollFontSize = properties.departmentNameSize;

            int refresh = properties.refresh * 1000;

            lDataFragmentLayout.setBackgroundColor(templateBackgroundColorCode);
            tvScrollText.setBackgroundColor(departmentHeaderBackgroundColorCode);
            tvScrollText.setTextColor(scrollTextColorCode);
            tvScrollText.setTextSize(TypedValue.COMPLEX_UNIT_SP, scrollFontSize);
            tvScrollText.setText(doctorDirectories.scrollText);
            tvScrollText.setTypeface(typefaceBold);


            customList = doctorDirectories.customDoctorLists;


            DirectoryAdapter adapter = new DirectoryAdapter(DirectoryFragment.this.getActivity(), customList, properties,this.asset);
            adapterViewFlipper.setAdapter(adapter);
            adapterViewFlipper.setFlipInterval(refresh);
            adapterViewFlipper.startFlipping();

        }
    }

}





















