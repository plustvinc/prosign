package com.plustv.prosignsdk.fragments;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;

import java.io.File;

/**
 * Created by Rachit Solanki on 13/8/18.
 */
public class ImageFragment extends BaseFragment {

@Override
public int getLayout() {
        return R.layout.fragment_image;
}
ImageView imageView;
@Override
public void onCreateView(View view) {
        imageView = view.findViewById(R.id.imageView);
        File file = FileUtility.getFile(this.asset.name);


        /*
        * Check if file exits and if its lock doesn't exists
        * This is to avoid loading image by Glide partially,
        * even when the downloading is still in progress.
        */
        if( file.exists() &&
            !FileUtility.isLockExists(this.asset.name)) {
                Glide.with(this)
                        .load(file)
                        .into(imageView);
        } else {
                Glide.with(this)
                        .load(R.drawable.default_image)
                        .into(imageView);
        }

}


@Override
public void onDetach() {
        imageView = null;
        super.onDetach();
}
}
