package com.plustv.prosignsdk.fragments;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.plustv.prosignsdk.ImageConverter;
import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;
import com.plustv.prosignsdk.models.ConsultantDirectory;
import com.plustv.prosignsdk.models.DoctorDetails;

import java.io.File;

public class ConsultantFragment extends BaseFragment {

private DoctorDetails doctorDetails;
public ConsultantDirectory consultantDirectory;
TextView doctName, doctQualification,doctDesignation ,roomNo ,tvDepartment,tvTokenNo;
//CircularImageView imgDoct;
ImageView imgDoct;


RequestOptions options = new RequestOptions()
	.centerCrop()
	.placeholder(R.drawable.error_thumnail)
	.error(R.drawable.error_thumnail)
	.diskCacheStrategy(DiskCacheStrategy.ALL)
	.priority(Priority.HIGH);



@Override
public int getLayout() {
	return R.layout.fragment_consultant_directory;
}


@Override
public void onCreateView(View view) {

	doctName = view.findViewById(R.id.tvDoctName);
	doctQualification = view.findViewById(R.id.tvDoctQualification);
	doctDesignation = view.findViewById(R.id.tvDoctDesign);
	roomNo = view.findViewById(R.id.tvRoomNo);
	tvDepartment = view.findViewById(R.id.tvDepartment);
	tvTokenNo = view.findViewById(R.id.tvTokenNo);
	imgDoct = view.findViewById(R.id.imgDoct);


	consultantDirectory = this.asset.consultantDirectory;
	if(consultantDirectory != null) {
		doctorDetails = consultantDirectory.doctorDetails;
		if (doctorDetails != null) {

			doctName.setText(doctorDetails.name);
			doctQualification.setText(doctorDetails.qualification);
			doctDesignation.setText(doctorDetails.designation);
			roomNo.setText(String.valueOf(doctorDetails.roomNumber));
			tvDepartment.setText(doctorDetails.department.getName());


			String imageSrc = doctorDetails.src;
			String filename = imageSrc.substring(imageSrc.lastIndexOf("/") + 1);

			File file = FileUtility.getFile(filename);
			Log.e("filename","filename --"+ filename+" file  "+file.getName()
			);


			  StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			  StrictMode.setThreadPolicy(policy);

			  // Bitmap myImage = getBitmapFromURL(imageSrc);
			  Bitmap myImage;
			  myImage = BitmapFactory.decodeFile(file.getAbsolutePath());
			  Log.e("consultant myImage"," consultant myImage  "+ myImage);

			  if(myImage == null) {
				    myImage = BitmapFactory.decodeResource(ConsultantFragment.this.getResources(), R.drawable.error_thumnail);
			  }



			  if (file.exists() &&
				    !FileUtility.isLockExists(file.getName())) {
				    Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(myImage, 1000);
				    imgDoct.setImageBitmap(circularBitmap);

			  }

			  else {
				    Bitmap bitmap = BitmapFactory.decodeResource(ConsultantFragment.this.getResources(), R.drawable.error_thumnail);
				    Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 1000);
				    imgDoct.setImageBitmap(circularBitmap);
			  }


		}
	}

  }

}







