package com.plustv.prosignsdk.fragments;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;

import java.io.File;
import java.io.IOException;

import static com.plustv.prosignsdk.common.FileUtility.getFile;

public class AudioFragment extends BaseFragment  {

FrameLayout parentView;
MediaPlayer mPlayer;
ImageView imgAudio;


@Override
public int getLayout() {
	  return R.layout.fragment_audio;
}


@Override
public void onCreateView(View view) {
	  parentView = (FrameLayout) view;
	  imgAudio = view.findViewById(R.id.imgAudio);


	  if (FileUtility.fileExists(asset.name) &&
		    !FileUtility.isLockExists(asset.name)) {

		    Glide.with(this)
				.load(R.drawable.music)
				.into(imgAudio);

		    File f = getFile(asset.name);
		    File url = f.getAbsoluteFile();
		    Log.e("audio ", "audio " + url.getAbsolutePath());
		    play(url);


	  }
	  else{
		    Glide.with(this)
				.load(R.drawable.default_image)
				.into(imgAudio);
	  	  Log.e(" check audio download"," check audio download ");
	  }

	}


   public void play(File url){
	     mPlayer = new MediaPlayer();
	     mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
	     try {
			 mPlayer.setDataSource(url.getAbsolutePath());
	     } catch (IOException e) {
			 e.printStackTrace();
	     }
	     try {
			 mPlayer.prepare();
	     } catch (IOException e) {
			 e.printStackTrace();
	     }
	     mPlayer.start();

   }


@Override
public void onDetach() {
	  imgAudio = null;
	  if(mPlayer!=null) {
		    mPlayer.stop();
		    mPlayer.release();
		    mPlayer = null;
	  }
	  super.onDetach();
}



}


