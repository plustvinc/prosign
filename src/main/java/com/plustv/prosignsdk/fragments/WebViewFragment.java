package com.plustv.prosignsdk.fragments;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;


public class WebViewFragment extends BaseFragment {

    @Override
    public int getLayout() {
              return  R.layout.web_view;
    }

@Override
@SuppressLint("SetJavaScriptEnabled")
    public void onCreateView(View view) {
        WebView webView = (WebView) view;
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            request.grant(request.getResources());
                        }
                    }
                });
            }

        });
        webView.setWebViewClient(new WebViewClient());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setWebContentsDebuggingEnabled(true);
        }
        webView.setLayerType(WebView.LAYER_TYPE_HARDWARE, null);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.clearCache(true);
        webView.getSettings().setUserAgentString("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0");

        if( this.asset.src.startsWith("http") ||
            this.asset.src.startsWith("https")) {
                webView.loadUrl(this.asset.src);
        } else {
                webView.loadUrl("file:///"+ FileUtility.getFile(this.asset.src).getAbsolutePath());
        }
    }


}
