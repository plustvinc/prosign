package com.plustv.prosignsdk.fragments;

import android.content.Intent;
import android.view.View;

import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.service.MusicService;

public class ModeFragment extends BaseFragment {
@Override
public int getLayout() { return R.layout.fragment_fm; }


@Override
public void onCreateView(View view) {

	  String mode = this.asset.assetProperty.mode;
	  if(mode.equalsIgnoreCase("background")){
		    getActivity().startService(new Intent(getActivity(), MusicService.class));

	  }

}
}
