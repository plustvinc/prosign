package com.plustv.prosignsdk.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;
import com.plustv.prosignsdk.models.Properties;
import com.plustv.prosignsdk.view.DigitalClock;
import com.plustv.prosignsdk.view.MyVectorClock;

import java.util.Calendar;


/**
 * Created by PlusTv on 7/2/17.
 */

public class ClockFragment extends BaseFragment {

private FrameLayout flClockParent;

@Override
public int getLayout() {
return R.layout.fragment_clock;
}

@Override
public void onCreateView(final View view) {

        flClockParent = view.findViewById(R.id.flClockParent);

        if(asset == null || asset.assetProperty == null)
                return;

        Properties spanProperties=this.asset.assetProperty;
        flClockParent.setBackgroundColor(parseColor(spanProperties.bg));

        if("digital".equalsIgnoreCase(spanProperties.mode)) {
                final DigitalClock digitalClock = view.findViewById(R.id.digitalClock);
                digitalClock.setVisibility(View.VISIBLE);
                digitalClock.setTextColor(Color.WHITE);
                view.getViewTreeObserver()
                        .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                /*Top and bottom padding*/
                                int fontSize = ((int) (digitalClock.getMeasuredHeight()/getScreenDensity())) - 16;
                                digitalClock.setTextSize(fontSize>100?100:fontSize);
                        }
                });
        }

        if("analog".equalsIgnoreCase(spanProperties.mode)) {
                Calendar calendar = Calendar.getInstance();
                MyVectorClock vectorAnalogClock = view.findViewById(R.id.clock);
                vectorAnalogClock.setVisibility(View.VISIBLE);
                vectorAnalogClock.setCalendar(calendar)
                        .setDiameterInDp(180.0f)
                        .setOpacity(1.0f)
                        .setShowSeconds(true)
                        .setColor(Color.WHITE);
        }


}

private Typeface typeface;

public void setFont(String font, TextView tv) throws Exception {
        typeface=Typeface.createFromFile(FileUtility.getFile(font));
        tv.setTypeface(typeface);
}


@Override
public void onStart() {
        super.onStart();
        ((ViewGroup)getView().getParent()).setBackgroundColor(Color.TRANSPARENT);
}

}
