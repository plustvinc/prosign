package com.plustv.prosignsdk.fragments;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.pwittchen.weathericonview.WeatherIconView;
import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.EndPointApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Rachit Solanki
 * @date 30/10/18
 */

//TODO : Cache weather response and time to avoid unnecessary calls
// Yahoo weather end of life

public class WeatherFragment extends BaseFragment {

@Deprecated
private final String YAHOO = "https://query.yahooapis.com/v1/public/yql?u={UNIT}&q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22{CITY}%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

ImageView imageView;
TextView tvTemperature;
TextView tvCity, tvUnit, tvDay, tvDate, tvStatus;
TextView tvDatesArray[] = new TextView[6];
TextView tvStatusArray[] = new TextView[6];
WeatherIconView weatherArray[] = new WeatherIconView[6];
WeatherIconView weatherIconView;


final String TILE_SCREEN_MODE = "tile";
final String FULL_SCREEN_MODE = "full_screen";

String weatherViewType = "none";

@Override
public int getLayout() {
        weatherViewType = asset.assetProperty.mode;
        switch (weatherViewType){
                case FULL_SCREEN_MODE : return R.layout.fragment_weather_7_days;
                case TILE_SCREEN_MODE :
                default               : return R.layout.fragment_weather_1_days;
        }
}
RequestOptions requestOptions = new RequestOptions();
@Override
public void onCreateView(View view) {
        // common views
        imageView = view.findViewById(R.id.backgroundImage);
        tvTemperature = view.findViewById(R.id.tvTemperature);
        weatherIconView = view.findViewById(R.id.weatherIconView);
        tvCity  = view.findViewById(R.id.tvCity);
        tvDay = view.findViewById(R.id.tvDay);
        tvUnit = view.findViewById(R.id.tvUnit);
        tvDate = view.findViewById(R.id.tvDate);
        tvStatus  = view.findViewById(R.id.tvStatus);

       if(FULL_SCREEN_MODE.equalsIgnoreCase(weatherViewType) ) {
                populate7Days(view);
               requestOptions.placeholder(R.drawable.default_image);
               requestOptions.error(R.drawable.default_image);
               Glide.with(this)
                        .setDefaultRequestOptions(requestOptions)
                        .load( asset.assetProperty!=null ? asset.assetProperty.src : "")
                        .into(imageView);

        }

        switch (asset.assetProperty.unit) {
                case "fahrenheit" : tvUnit.setText(getString(R.string.fahrenheit));
                                    break;
                case "celsius"    :
                default           : tvUnit.setText(getString(R.string.celsius));
        }

        getWeather(asset.assetProperty.city);
}

private void populate7Days(View view) {

        tvDatesArray[0] = view.findViewById(R.id.tvDate1);
        tvDatesArray[1] = view.findViewById(R.id.tvDate2);
        tvDatesArray[2] = view.findViewById(R.id.tvDate3);
        tvDatesArray[3] = view.findViewById(R.id.tvDate4);
        tvDatesArray[4] = view.findViewById(R.id.tvDate5);
        tvDatesArray[5] = view.findViewById(R.id.tvDate6);

        tvStatusArray[0] = view.findViewById(R.id.tvStatus1);
        tvStatusArray[1] = view.findViewById(R.id.tvStatus2);
        tvStatusArray[2] = view.findViewById(R.id.tvStatus3);
        tvStatusArray[3] = view.findViewById(R.id.tvStatus4);
        tvStatusArray[4] = view.findViewById(R.id.tvStatus5);
        tvStatusArray[5] = view.findViewById(R.id.tvStatus6);

        weatherArray[0] = view.findViewById(R.id.weather1);
        weatherArray[1] = view.findViewById(R.id.weather2);
        weatherArray[2] = view.findViewById(R.id.weather3);
        weatherArray[3] = view.findViewById(R.id.weather4);
        weatherArray[4] = view.findViewById(R.id.weather5);
        weatherArray[5] = view.findViewById(R.id.weather6);

}

private void getWeather(String cityName) {
        if(cityName==null) {
                return;
        }

        tvCity.setText(cityName);
        Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl("http://192.168.1.7:9000/")
                                        .addConverterFactory(GsonConverterFactory.create())
                                        .build();

        EndPointApi api = retrofit.create(EndPointApi.class);

        String url =   YAHOO.replace("{CITY}",cityName);
        url = url.replace("{UNIT}",asset.assetProperty.unit);
        Call<ResponseBody> call = api.yahooWeather(url);
        call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call,
                                       @NonNull Response<ResponseBody> response) {
                        if(response.isSuccessful()) {
                                try {
                                        if(!isDetached())
                                                parseData(new JSONObject(response.body().string()));
                                } catch (JSONException | IOException e) {
                                        e.printStackTrace();
                                }
                        }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
        });
}

private void parseData(JSONObject mainJSON) {
        try {

                JSONObject channelJSONObject = mainJSON.getJSONObject("query").getJSONObject("results").getJSONObject("channel");

                JSONObject itemJSONObject = channelJSONObject.getJSONObject("item");
                JSONArray forecastJSONArray = itemJSONObject.getJSONArray("forecast");

                if(forecastJSONArray.length()<=0) {
                        return;
                }

                String descriptionString = itemJSONObject.getString("description");
                String imgUrl = new StringBuilder(descriptionString).substring(descriptionString.indexOf("<img src=\"") + "<img src=\"".length(), descriptionString.lastIndexOf("\"/>"));

                if (imgUrl.length() == 37) {
                        weatherIconView.setIconResource(parseIcon(imgUrl.substring(31, 37)));
                } else if (imgUrl.length() == 36) {
                        weatherIconView.setIconResource(parseIcon(imgUrl.substring(31, 36)));
                }

                JSONObject currentDateObject = forecastJSONArray.getJSONObject(0);
                tvDay.setText(currentDateObject.getString("day"));
                tvDate.setText(currentDateObject.getString("date"));
                tvStatus.setText(currentDateObject.getString("text"));

                switch (asset.assetProperty.unit) {
                        case "fahrenheit" : tvTemperature.setText( parseTemp(currentDateObject)+"" );
                                break;
                        case "celsius"    :
                        default           : tvTemperature.setText( ((int)((parseTemp(currentDateObject) - 32) / 1.8f))+"" );
                }



                if( FULL_SCREEN_MODE.equalsIgnoreCase( weatherViewType ) &&
                    forecastJSONArray.length() > weatherArray.length ) {

                        for(int i=0;i< weatherArray.length ;i++) {
                                JSONObject jsonObject = forecastJSONArray.getJSONObject(i+1);
                                tvStatusArray[i].setText(jsonObject.getString("text"));
                                weatherArray[i].setIconResource(parseIcon(jsonObject.getString("code")+".gif"));
                                tvDatesArray[i].setText(jsonObject.getString("date"));
                        }
                }

        } catch (JSONException e) {
                e.printStackTrace();
        }

}

int parseTemp(JSONObject dayWeatherJSONObject) throws JSONException {
        int low = Integer.parseInt(dayWeatherJSONObject.getString("low"));
        int high = Integer.parseInt(dayWeatherJSONObject.getString("high"));
        int avg = (high+low)/2;
        if(high < low) {
                avg = low;
        }

        return avg;
}

private String parseIcon(String iconId) {
        String icon = getString(R.string.wi_na);
        switch (iconId) {
                //tornado
                case "0.gif" : icon=getString(R.string.wi_tornado);
                        break;
                //tropical storm
                case "1.gif" : icon=getString(R.string.wi_sandstorm);
                        break;
                //hurricane
                case "2.gif" : icon=getString(R.string.wi_hurricane);
                        break;
                //severe thunderstorms
                case "3.gif" : icon=getString(R.string.wi_storm_showers);
                        break;
                // thunderstorms
                case "4.gif" : icon=getString(R.string.wi_lightning);
                        break;
                //mixed rain and snow
                case "5.gif" :
                //mixed rain and sleet
                case "6.gif" :
                // mixed snow and sleet
                case "7.gif" : icon=getString(R.string.wi_snow);
                        break;
                //freezing drizzle
                case "8.gif" :
                // drizzle
                case "9.gif" : icon=getString(R.string.wi_snowflake_cold);
                        break;
                // freezing rain
                case "10.gif" : icon=getString(R.string.wi_snow);
                        break;
                // showers
                case "11.gif" :
                // freezing rain
                case "12.gif" :
                // snow flurries
                case "13.gif" :
                // light snow showers
                case "14.gif" : icon=getString(R.string.wi_showers);
                        break;
                //snow
                case "16.gif" :
                //hail
                case "17.gif" :
                //sleet
                case "18.gif" :
                // blowing snow
                case "15.gif" : icon=getString(R.string.wi_snow);
                        break;
                // dust
                case "19.gif" :
                        icon=getString(R.string.wi_dust);
                        break;
                // foggy
                case "20.gif" :
                // haze
                case "21.gif" :
                // smoky
                case "22.gif" :
                // blustery
                case "23.gif" :
                        icon=getString(R.string.wi_fog);
                        break;
                // windy
                case "24.gif" :
                        icon=getString(R.string.wi_windy);
                        break;
                // cold
                case "25.gif" :
                        icon=getString(R.string.wi_snowflake_cold);
                        break;
                // cloudy
                case "26.gif" :
                        icon=getString(R.string.wi_cloudy);
                        break;
                // mostly cloudy (night)
                case "27.gif" :
                        icon=getString(R.string.wi_night_alt_cloudy);
                        break;
                // mostly cloudy (day)
                case "28.gif" :
                        icon=getString(R.string.wi_day_cloudy);
                        break;
                // partly cloudy (night)
                case "29.gif" :
                        icon=getString(R.string.wi_night_alt_cloudy);
                        break;
                // partly cloudy (day)
                case "30.gif" :
                        icon=getString(R.string.wi_day_cloudy);
                        break;
                // clear (night)
                case "31.gif" :
                        icon=getString(R.string.wi_night_clear);
                        break;
                // sunny
                case "32.gif" :
                        icon=getString(R.string.wi_day_sunny);
                        break;
                // fair (night)
                case "33.gif" :
                        icon=getString(R.string.wi_night_alt_cloudy);
                        break;
                // fair (day)
                case "34.gif" :
                        icon=getString(R.string.wi_day_cloudy);
                        break;
                // mixed rain and hail
                case "35.gif" :
                        icon=getString(R.string.wi_rain_mix);
                        break;
                // hot
                case "36.gif" :
                        icon=getString(R.string.wi_hot);
                        break;
                // isolated thunderstorms
                case "37.gif" :
                // scattered thunderstorms
                case "38.gif" :
                // scattered thunderstorms
                case "39.gif" :
                // scattered thunderstorms
                case "40.gif" :
                        icon=getString(R.string.wi_storm_showers);
                        break;
                // heavy snow
                case "41.gif" :
                // scattered snow showers
                case "42.gif" :
                // heavy snow
                case "43.gif" :
                        icon=getString(R.string.wi_snow_wind);
                        break;
                // partly cloudy
                case "44.gif" :
                        icon=getString(R.string.wi_cloudy);
                        break;
                //thundershowers
                case "45.gif" :
                // snow showers
                case "46.gif" :
                // isolated thundershowers
                case "47.gif" :
                        icon=getString(R.string.wi_storm_showers);
                        break;

                case "3200.gif" : icon=getString(R.string.wi_degrees);
                        break;

                default: /* None */
        }
       return icon;
}


}
