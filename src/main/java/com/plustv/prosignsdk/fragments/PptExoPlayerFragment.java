package com.plustv.prosignsdk.fragments;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;
import com.plustv.prosignsdk.models.Asset;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Objects;

import static com.plustv.prosignsdk.common.FileUtility.getFile;


public class PptExoPlayerFragment extends BaseFragment {

    @Override
    public int getLayout() {
        return R.layout.fragment_exo_player_view;
    }

    SimpleExoPlayer player;
    PlayerView simpleExoPlayerView;
    DataSource.Factory dataSourceFactory;
    boolean isSequence = false;
    ArrayList<Asset> assetsList;
    MediaSource videoSource;
    LinearLayout parentLayout;
    @Override
    public void onCreateView(View view) {
        parentLayout = (LinearLayout) view;
        simpleExoPlayerView = view.findViewById(R.id.exo_player_video);

        if(getArguments()!=null) {
            assetsList = getArguments().getParcelableArrayList("assetsList");
            isSequence = getArguments().getBoolean("isSequence");
        }

        simpleExoPlayerView.requestFocus();
        simpleExoPlayerView.setUseController(false);

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        DefaultTrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        dataSourceFactory = new DefaultDataSourceFactory( Objects.requireNonNull(getContext()),
                                                          Util.getUserAgent( getContext(),
                                                                "ProSigCloud")
                                                         );

        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);


        simpleExoPlayerView.setPlayer(player);

        if(isSequence) {
            videoSource = createMultipleMediaSource();
        } else {
            videoSource = createSingleMediaSource(this.asset);
        }

        simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

        player.prepare(videoSource);
        player.setPlayWhenReady(true);


    }

    private MediaSource createMultipleMediaSource() {
        MediaSource mediaSource[] = new MediaSource[assetsList.size()];
        for(int i=0 ; i< assetsList.size();i++ ) {
            Asset a = assetsList.get(i);
            MediaSource videoSource = createSingleMediaSource(a);
            mediaSource[i] = videoSource;
        }
        simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);


        return new ConcatenatingMediaSource(mediaSource);
    }

    private MediaSource createSingleMediaSource(Asset a) {
        Uri uri = getAssetUri(a);
        return new ExtractorMediaSource.Factory(dataSourceFactory)
                                       .createMediaSource(uri);
    }

    private Uri getAssetUri(Asset a) {
        Uri uri;
        if( FileUtility.fileExists(a.name+".mp4") &&
            !FileUtility.isLockExists(a.name+".mp4")) {
                uri = Uri.parse( getFile(a.name+".mp4").getAbsolutePath() );

        } else {

		    uri = Uri.parse("rawresource:///" + R.raw.default_vid);

		    player.setRepeatMode(Player.REPEAT_MODE_ONE);
        }
        return uri;
    }

    @Override
    public void onDetach() {
        if(player!=null) {
            player.stop();
            player.release();
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                parentLayout.removeAllViews();
            }
        }
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    public void onDownload(String name) {
        int index = player.getCurrentWindowIndex();
        Asset asset = assetsList.get(index);
        String assetName = asset.name +".mp4";
        if(assetName.equals(name+".mp4")) {
            videoSource = createMultipleMediaSource();
            player.prepare(videoSource);
            player.seekTo(index,0);
            player.setPlayWhenReady(true);

	  }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String event) {
            // only for sequence
            if(isSequence) {
                onDownload(event);
            }
    }

}
