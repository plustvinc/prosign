package com.plustv.prosignsdk.fragments;

import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.ScrollingTextView;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;
import com.plustv.prosignsdk.common.FragmentPoolManager;
import com.plustv.prosignsdk.models.Properties;


/**
 * @author plustv-dev-0
 */
public class ScrollTextFragment extends BaseFragment {

//TextView scrollingTextView;
ScrollingTextView scrollingTextView;
long duration;

@Override
public int getLayout() {
        return R.layout.scroll_fragment;
}

Rect bounds = new Rect();
@Override
public void onCreateView(final View view) {

        scrollingTextView =  view.findViewById(R.id.scrollText);
        final Properties spanProperties = this.asset.assetProperty;
	 // duration = getArguments().getLong("duration")*60*1000;
	  duration = (spanProperties.rate)*1000;  //sec

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        /*Top and bottom padding*/
                        if(asset.type == FragmentPoolManager.SCROLL_TEXT ) {
                                //int fontSize = ((int) (scrollingTextView.getMeasuredHeight()/getScreenDensity())) - 6;
                                //scrollingTextView.setTextSize(fontSize>100?100:fontSize);
                                scrollingTextView.setTextSize(spanProperties.fontSize);
                                /*
                                 * A bit hack but don't want to use scrollingtextview
                                 * from package due to it's multilingual problem
                                 */
                                   if(bounds.width() < view.getWidth()) {
                                        int numOfTabs = (view.getWidth()-bounds.width())/4;
                                        StringBuilder string = new StringBuilder(asset.src);
                                        while (numOfTabs>0) {
                                                string.append("\t");
                                                numOfTabs-=4;
                                        }
                                        scrollingTextView.setText(string.toString());

                                }
                        }
                }
        });


        scrollingTextView.setTextColor(parseColor(spanProperties.color));
        scrollingTextView.setBackgroundColor(parseColor(spanProperties.bg));
	  scrollingTextView.setRndDuration(duration);
        scrollingTextView.setTextSize(spanProperties.fontSize);



        if(asset.type == FragmentPoolManager.SCROLL_TEXT ) {
//                scrollingTextView.setSelected(true);
                scrollingTextView.startScroll();
                scrollingTextView.setGravity(Gravity.CENTER_VERTICAL);
                if ("rtl".equalsIgnoreCase(spanProperties.direction)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                scrollingTextView.setTextDirection(View.TEXT_DIRECTION_RTL);
                        }
                }
        } else {
                // Static text

                scrollingTextView.setSingleLine(false);
                scrollingTextView.setTextSize(spanProperties.fontSize);
                scrollingTextView.setPadding(8,8,8,8);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                       scrollingTextView.setText(Html.fromHtml(asset.src+"",Html.FROM_HTML_OPTION_USE_CSS_COLORS ));
                }else {
                       scrollingTextView.setText(Html.fromHtml(asset.src+""));
                }
        }


        scrollingTextView.getPaint().getTextBounds(scrollingTextView.getText().toString(),
                                                0,
                                                  scrollingTextView.getText().length(), bounds);


}

@Override
public void onStart() {
    super.onStart();
    ((ViewGroup)getView().getParent()).setBackgroundColor(Color.TRANSPARENT);

}

private void updateFont(Properties spanProperties) {
        try {
                Typeface typeface = Typeface.createFromFile(FileUtility.getFile(spanProperties.fontFile));
                scrollingTextView.setTypeface(typeface);
        } catch (Exception e) {
                e.printStackTrace();
        }

}

}
