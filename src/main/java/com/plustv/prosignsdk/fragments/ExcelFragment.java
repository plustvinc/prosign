package com.plustv.prosignsdk.fragments;

import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FileUtility;

import static com.plustv.prosignsdk.common.FileUtility.getFile;


/**
 * Created by Rachit Solanki on 19/8/17.
 */

public class ExcelFragment extends BaseFragment {
Uri uri;

    @Override
    public int getLayout() {
        return R.layout.fragment_excel;
    }

    @Override
    public void onCreateView(View view) {

    WebView webView = (WebView) this.view;
    webView.setWebChromeClient(new WebChromeClient());
    webView.setWebViewClient(new WebViewClient());
    webView.getSettings().setJavaScriptEnabled(true);
    webView.getSettings().setUserAgentString("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0");

            if( FileUtility.fileExists(asset.name +".html") &&
                    !FileUtility.isLockExists(asset.name + ".html")) {
                    uri = Uri.parse(getFile(asset.name+".html").getAbsolutePath());
                    Log.e(" excel"," excel "+ uri);
                    webView.loadUrl("file:///" + uri);



            } else {
                    webView.loadUrl("file:///android_res/raw/default_page.html");

            }


    }


}



