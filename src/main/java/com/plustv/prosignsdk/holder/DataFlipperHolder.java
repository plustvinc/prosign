package com.plustv.prosignsdk.holder;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;


public  class DataFlipperHolder  {

    public TextView tvDetails, tvDepartment;
    public LinearLayout lDetailsLayout,lImageLayout,lDoctLayout,lTopLayout;
    public ImageView imageView;
    public CircularImageView circularImageView;
    public LinearLayout lDepartmentLayout;



}
