package com.plustv.prosignsdk.handler;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.plustv.prosignsdk.common.AnimationFactory;
import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.EventEmitter;
import com.plustv.prosignsdk.common.EventType;
import com.plustv.prosignsdk.common.FragmentPoolManager;
import com.plustv.prosignsdk.models.Asset;
import com.plustv.prosignsdk.models.RegionContainer;

import java.util.ArrayList;
import java.util.Map;
import java.util.Queue;

import static com.plustv.prosignsdk.common.FragmentPoolManager.VIDEO;
import static com.plustv.prosignsdk.common.FragmentPoolManager.EXO;
import static com.plustv.prosignsdk.common.FragmentPoolManager.VIDEO_PLAYER;

/**
 * Created by Rachit Solanki on 13/8/18.
 *
 * To change view fragment by providing assets.
 */
public class FragmentTransactionHandler extends Handler {

private EventEmitter eventEmitter;
private Map<Integer, RegionContainer> regionQueue;
private FragmentManager fragmentManager;
String TAG = "FragmentTransactionHandler";

public FragmentTransactionHandler( EventEmitter eventEmitter,
                                   Map<Integer, RegionContainer > regionQueue,
                                   FragmentManager fragmentManager) {
        this.eventEmitter=eventEmitter;
        this.regionQueue = regionQueue;
        this.fragmentManager = fragmentManager;
}

@Override
public void handleMessage(Message msg) {
        int regionId = msg.what;

        if(this.regionQueue.containsKey(regionId) ) {
                Queue<Asset> assetQueue = this.regionQueue.get(regionId).assetQueue;

                if (assetQueue == null || assetQueue.size() == 0) {
                        this.eventEmitter.emit(EventType.PLAYLIST_COMPLETE, regionId);
                } else {

                        BaseFragment oldFragment = (BaseFragment) this.fragmentManager
                                                                      .findFragmentById(regionId);

                        if (oldFragment != null) {
                                oldFragment.onDetach();
                        }

                        Asset asset = assetQueue.poll();
                        long duration = asset.duration*1000;
                        BaseFragment baseFragment = FragmentPoolManager.getInstance()
                                                                       .getFragmentFor(asset.type);

                        if(    asset.type == VIDEO &&
                               VIDEO_PLAYER.equals(EXO) ) {
                               ArrayList<Asset> assets = getAssetList(assetQueue);
                               if(assets.size()>0) {
                                       ArrayList<Asset> assetsVideoList = new ArrayList<>();
                                       for(Asset a : assets) {
                                               if(a.type != VIDEO) {
                                                        break;
                                               }

                                               duration += (a.duration * 1000);
                                               assetsVideoList.add(a);
                                       }
                                       assetsVideoList.add(0,asset);
                                       if(assetsVideoList.size()>1) {
                                               Bundle bundle = new Bundle();
                                               bundle.putBoolean("isSequence",true);
                                               bundle.putParcelableArrayList("assetsList",
                                                                             assetsVideoList);

                                               baseFragment.setArguments(bundle);
                                        }


                               }
                        }

                        baseFragment.setAsset(asset);
                        FragmentTransaction ft = this.fragmentManager.beginTransaction();
                        AnimationFactory.getInstance()
                                        .setAnimation(1,ft);
                                ft.replace(regionId, baseFragment)
                                .commit();


                        // dispatch event
                        this.eventEmitter.emit(EventType.ASSET_PLAYING,asset);

                        // if asset duration is 0 let it run for indefinite
                        if ( duration != 0 ) {
                                sendEmptyMessageDelayed(regionId, duration);
                        }

                        return;
                }

        }

        sendEmptyMessageDelayed(regionId,1000);
}



ArrayList<Asset> getAssetList(Queue<Asset> assetQueue) {
        ArrayList<Asset> assetArrayList = new ArrayList<>();

        for(Object a : assetQueue.toArray() ) {
                if(((Asset)a).type != VIDEO) {
                        break;
                }
                assetQueue.poll();
                assetArrayList.add((Asset)a);
        }

        return assetArrayList;

}


}
