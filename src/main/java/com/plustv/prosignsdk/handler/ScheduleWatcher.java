package com.plustv.prosignsdk.handler;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.plustv.prosignsdk.common.EndPointApi;
import com.plustv.prosignsdk.common.EventEmitter;
import com.plustv.prosignsdk.common.FileUtility;
import com.plustv.prosignsdk.common.NetworkFactory;
import com.plustv.prosignsdk.models.MappedSchedule;
import com.plustv.prosignsdk.models.Schedule;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

import static com.plustv.prosignsdk.common.DefaultSchedule.getDefaultSchedule;
import static com.plustv.prosignsdk.common.EventType.SCHEDULE_CHANGED;

/**
 * Created by Rachit Solanki on 13/8/18.
 */
public class ScheduleWatcher extends Handler {

private EventEmitter eventEmitter;
private String deviceUUID;

private final int DO_NOTHING = 0;
private final int FILE = 1;
private final int URL = 2;
private int scheduleType;
private Schedule existingSchedule = null;
private final String TAG = "ScheduleWatcher";

public ScheduleWatcher(@NonNull EventEmitter eventEmitter,
                       @NonNull String deviceUUID) {

        this.eventEmitter=eventEmitter;
        this.deviceUUID=deviceUUID;
        this.scheduleType = URL;
}



@Override
public void handleMessage(Message msg) {
        switch (scheduleType) {
                case FILE : watchScheduleFile();
                            sendEmptyMessageDelayed(msg.what,1000);
                           break;
                case URL : downloadScheduleURL();
                           sendEmptyMessageDelayed(msg.what,30000);
                           break;
                case DO_NOTHING :
                               //  eventEmitter.emit(SCHEDULE_CHANGED,this.existingSchedule);
                               //  break;
                 default: /*Do nothing*/
        }


}

@Deprecated
private int getApiType(String deviceUUID) {
        if(deviceUUID.startsWith("http") || deviceUUID.startsWith("https")) {
                return URL;
        }

        if(deviceUUID.startsWith("file")) {
                return FILE;
        }

        return DO_NOTHING;
}


private void downloadScheduleURL() {

        EndPointApi interfaceCallback = NetworkFactory.createNetworkInterface(EndPointApi.class);
        Call<MappedSchedule> call = interfaceCallback.getScheduleJsonByUUID(this.deviceUUID);
        call.enqueue(new retrofit2.Callback<MappedSchedule>() {
                @Override
                public void onResponse(@NonNull Call<MappedSchedule> call,
                                       @NonNull Response<MappedSchedule> response) {
                        if(response.isSuccessful()) {
                                MappedSchedule mappedSchedule = response.body();
                                if(mappedSchedule == null || !mappedSchedule.isAssigned()) {
                                        /*
                                         * Play Default here
                                         * we will not break flow just attach our default_page
                                         * schedule here
                                         */
                                        mappedSchedule = new MappedSchedule();

                                        mappedSchedule.setSchedule(getDefaultSchedule());
                                        // Now assigned
                                        mappedSchedule.setAssigned(true);
                                }

                                Schedule newSchedule = mappedSchedule.getSchedule();
                                if (isScheduleSame(newSchedule)) {
                                        existingSchedule = newSchedule;
                                        eventEmitter.emit(SCHEDULE_CHANGED,existingSchedule);
                                }
                                try {
                                        if( newSchedule != null ) {
                                                FileUtility.stringToFile(new Gson().toJson(newSchedule),
                                                        new File(FileUtility.getScheduleName()));
                                        }

                                }catch (IOException e) {
                                        e.printStackTrace();
                                        Log.e("Error in write ",
                                              "Schedule file json error "+e.getMessage());
                                }


                        } else {
                                pickFromScheduleFile();
                                Log.e("Response not","Successfull");
                        }
                }

                @Override
                public void onFailure(@NonNull Call<MappedSchedule> call,
                                      @NonNull  Throwable t) {
                        Log.e("schedule exception","schedule exception  "+ t);
                        pickFromScheduleFile();
                }
        });
}

private void pickFromScheduleFile() {
        try {

                Schedule newSchedule = parseSchedule(new File(FileUtility.getScheduleName()));
                if (isScheduleSame(newSchedule)) {
                        existingSchedule = newSchedule;
                        eventEmitter.emit(SCHEDULE_CHANGED,existingSchedule);
                }
        } catch (IOException e) {
                e.printStackTrace();
        }
}

private void watchScheduleFile() {
        File file = new File(deviceUUID);
        // can't do anything
        if(!file.exists()) {
                return;
        }

        try {
                Schedule newSchedule = parseSchedule(file);
                if (isScheduleSame(newSchedule)) {
                        this.existingSchedule = newSchedule;
                        this.eventEmitter.emit(SCHEDULE_CHANGED,this.existingSchedule);
                }

        } catch (IOException e) {
                Log.e(TAG,"File parse error "+e.getMessage());
        }

}


private boolean isScheduleSame(Schedule newSchedule) {
        return existingSchedule == null ||
                !existingSchedule.md5checksum.equals(newSchedule.md5checksum);
}

/**
 *
 * @param schedule file consisting json
 * @return Schedule object
 * @throws IOException exception
*/
private Schedule parseSchedule(File schedule) throws IOException {
        return parseSchedule(FileUtility.fileToString(schedule));
}

/**
 *
 * @param string json string
 * @return  instance of Schedule
 */
private Schedule parseSchedule(String string) {
        return new Gson().fromJson(string,Schedule.class);
}


}
