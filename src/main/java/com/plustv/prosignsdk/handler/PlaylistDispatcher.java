package com.plustv.prosignsdk.handler;

import android.os.Handler;
import android.os.Message;

import com.plustv.prosignsdk.common.EventEmitter;
import com.plustv.prosignsdk.common.EventType;
import com.plustv.prosignsdk.models.Presentation;

import java.util.Queue;

/**
 * @author
 * @date
  * Dispatch presentation to be played.
 * <p>
 * General Flow
 *   [p1] ( x removed )
 *   [p2] <- Polls and wait for presentation to complete.
 *   [p3]
 * </p>
 */
public class PlaylistDispatcher extends Handler {

/**
* Event emitter to dispatch events
*/
private EventEmitter eventEmitter;
private Queue<Presentation> presentationQueue;
private int repeatPresentationId = -1;
public PlaylistDispatcher(EventEmitter eventEmitter, Queue<Presentation> presentationQueue) {
    this.eventEmitter=eventEmitter;
    this.presentationQueue = presentationQueue;
    repeatPresentationId = -1;
}

@Override
public void handleMessage(Message msg) {
    int holderId = msg.what;

    if(this.presentationQueue !=null && this.presentationQueue.size() > 0 ) {
        Presentation presentation = this.presentationQueue.poll();
        this.eventEmitter.emit(EventType.PRESENTATION_STARTED, presentation);

        /*  Handle Start time later.
         *  To stop at presentation from being changed
         *  check if this is repeatedPresentation or
         *  Ignore if presentation duration is 0
         */
        if( presentation.duration !=0 &&
            repeatPresentationId != presentation.id ) {
            sendEmptyMessageDelayed(holderId, presentation.duration*1000);
        }
        return;
    }

      this.eventEmitter.emit(EventType.SCHEDULE_FINISHED,null);
}

public Queue<Presentation> getPlayList(){
        return presentationQueue;
}

public void setRepeatPresentation(int repeatPresentationId) {
    this.repeatPresentationId = repeatPresentationId;
}

}
