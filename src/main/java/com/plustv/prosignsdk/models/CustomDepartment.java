package com.plustv.prosignsdk.models;

import java.util.ArrayList;
import java.util.List;

public class CustomDepartment {

    public int id;
    public String name;
    public List<CustomDoctor> doctorDetails = new ArrayList<>();

}
