package com.plustv.prosignsdk.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Rachit Solanki on 13/8/18.
 * 192.168.1.108
 */
public class Schedule {

public int id;

@Expose
@SerializedName("presentation")
public List<Presentation> presentation;
@Expose
@SerializedName("assetToDownload")
public List<Asset> assetToDownload;

public boolean fromFile = false;
@Expose
@SerializedName("checksum")
public String md5checksum;

@Expose
@SerializedName("name")
public String name;

@Expose
@SerializedName("duration")
public long duration;

@Expose
@SerializedName("repeatedPresentation")
public int repeatedPresentation;

}
