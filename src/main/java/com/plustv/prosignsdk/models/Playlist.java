package com.plustv.prosignsdk.models;

import java.util.List;

/**
 * Created by Rachit Solanki on 21/9/18.
 */
public class Playlist {

public int id;
public String name;
public long duration;
public List<Asset> assets;

public int getId() {
        return id;
}

public void setId(int id) {
        this.id = id;
}

public String getName() {
        return name;
}

public void setName(String name) {
        this.name = name;
}

public long getDuration() {
        return duration;
}

public void setDuration(long duration) {
        this.duration = duration;
}

public List<Asset> getAssets() {
        return assets;
}

public void setAssets(List<Asset> assets) {
        this.assets = assets;
}

}
