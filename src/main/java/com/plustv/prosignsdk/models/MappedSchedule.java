package com.plustv.prosignsdk.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rachit Solanki on 21/9/18.
 */
public class MappedSchedule {

@Expose
@SerializedName("assigned")
private boolean assigned;
@Expose
@SerializedName("schedule")
private Schedule schedule;

public void setAssigned(boolean assigned) {
        this.assigned = assigned;
}

public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
}

public boolean isAssigned() {
        return assigned;
}
public Schedule getSchedule() {
        return schedule;
}

}
