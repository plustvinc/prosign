package com.plustv.prosignsdk.models;

public class ConsultantDirectory {

public int id;
public String name;
public DoctorDetails doctorDetails;


public int getId() { return id; }

public void setId(int id) { this.id = id; }

public String getName() { return name; }

public void setName(String name) { this.name = name; }

public DoctorDetails getDoctorDetails() { return doctorDetails; }

public void setDoctorDetails(DoctorDetails doctorDetails) { this.doctorDetails = doctorDetails; }


}
