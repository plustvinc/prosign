package com.plustv.prosignsdk.models;

import java.util.ArrayList;
import java.util.List;

public class DoctorDirectory {
    public int id;
    public String name;
    public String checksum;
    public String scrollText;
    public List<CustomDoctorList> customDoctorLists =new ArrayList<>();


    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getChecksum() { return checksum; }

    public void setChecksum(String checksum) { this.checksum = checksum; }

    public String getScrollText() { return scrollText; }

    public void setScrollText(String scrollText) { this.scrollText = scrollText; }



    public List<CustomDoctorList> getCustomDoctorLists() { return customDoctorLists; }

    public void setCustomDoctorLists(List<CustomDoctorList> customDoctorLists) { this.customDoctorLists = customDoctorLists; }


}
