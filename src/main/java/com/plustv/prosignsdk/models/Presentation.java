package com.plustv.prosignsdk.models;

import java.util.List;

/**
 * Created by Rachit Solanki on 13/8/18.
 */
public class Presentation {

public String name;
public int id;
public List<Region> regions;
public String startTime;

public String endTime;

/**
 In secs
 Handler will convert to milli secs
*/
public long duration;

}
