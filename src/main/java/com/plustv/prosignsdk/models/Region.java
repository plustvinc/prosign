package com.plustv.prosignsdk.models;

import com.google.gson.annotations.SerializedName;
import com.plustv.prosignsdk.common.RegionType;

import java.util.List;

public class Region {

@SerializedName("height")
private int height;
@SerializedName("width")
private int width;
@SerializedName("top")
private int top;
@SerializedName("left_pos")
private int left;
@SerializedName("zIndex")
private int zIndex;
@SerializedName("id")
private int id;
@SerializedName("playlist")
private Playlist playlist;
@SerializedName("regionType")
private RegionType regionType;
@SerializedName("assets")
private List<Asset> assets;

@SerializedName("colour")
private String colour;

public int getHeight() {
        return height;
}

public void setHeight(int height) {
        this.height = height;
}

public int getWidth() {
        return width;
}

public void setWidth(int width) {
        this.width = width;
}

public int getTop() {
        return top;
}

public void setTop(int top) {
        this.top = top;
}

public int getLeft() {
        return left;
}

public void setLeft(int left) {
        this.left = left;
}

public int getzIndex() {
        return zIndex;
}

public void setzIndex(int zIndex) {
        this.zIndex = zIndex;
}

public int getId() {
        return id;
}

public void setId(int id) {
        this.id = id;
}

public RegionType getRegionType() {
        return regionType;
}

public void setRegionType(RegionType regionType) {
        this.regionType = regionType;
}

public List<Asset> getAssets() {
        return assets;
}

public void setAssets(List<Asset> assets) {
        this.assets = assets;
}

public Playlist getPlaylist() {
        return playlist;
}

public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
}

public String getColour() {
        return colour;
}

public void setColour(String colour) {
        this.colour = colour;
}

}
