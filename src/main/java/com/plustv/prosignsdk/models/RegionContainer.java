package com.plustv.prosignsdk.models;

import com.plustv.prosignsdk.common.RegionType;

import java.util.Queue;

/**
 * @author Rachit Solanki
 * @date 14/8/18.
 *
 * Region Container consists region Id and its associated playlist
 * Which needs to be played on this region
 */
public class RegionContainer {
public RegionType regionType;
public int playlistId;
public String playlistName;
public Queue<Asset> assetQueue;

public RegionContainer(RegionType regionType, Queue<Asset> assetQueue) {
        this.regionType = regionType;
        this.assetQueue = assetQueue;
}

public RegionContainer(RegionType regionType, Queue<Asset> assetQueue,
                       int playlistId, String playlistName) {
        this.regionType = regionType;
        this.playlistId = playlistId;
        this.playlistName = playlistName;
        this.assetQueue = assetQueue;
}
}
