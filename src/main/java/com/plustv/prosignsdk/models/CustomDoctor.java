package com.plustv.prosignsdk.models;

public class CustomDoctor {

public int id;
public String name;
public String qualification;
public String designation;
public String time;
public String src;
public String admissionDate;
public int bedNumber=0;
public String roomNumber;
public int uhid;
public int floorNo=0;
public String gender;
public CustomDepartment department;


public int getId() { return id; }

public void setId(int id) { this.id = id; }

public String getName() { return name; }

public void setName(String name) { this.name = name; }

public String getQualification() { return qualification; }

public void setQualification(String qualification) { this.qualification = qualification; }

public String getDesignation() { return designation; }

public void setDesignation(String designation) { this.designation = designation; }

public String getTime() { return time; }

public void setTime(String time) { this.time = time; }

public String getSrc() { return src; }

public void setSrc(String src) { this.src = src;}


public String getAdmissionDate() { return admissionDate; }

public void setAdmissionDate(String admissionDate) { this.admissionDate = admissionDate; }

public int getBedNumber() { return bedNumber; }

public void setBedNumber(int bedNumber) { this.bedNumber = bedNumber; }

public String getRoomNumber() { return roomNumber; }

public void setRoomNumber(String roomNumber) { this.roomNumber = roomNumber; }

public int getUhid() { return uhid; }

public void setUhid(int uhid) { this.uhid = uhid; }

public int getFloorNo() { return floorNo; }

public void setFloorNo(int floorNo) { this.floorNo = floorNo; }

public String getGender() { return gender; }

public void setGender(String gender) { this.gender = gender; }

public CustomDepartment getDepartment() { return department; }

public void setDepartment(CustomDepartment department) { this.department = department; }


}
