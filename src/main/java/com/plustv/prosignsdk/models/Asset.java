package com.plustv.prosignsdk.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rachit Solanki on 13/8/18.
 */
public class Asset implements Parcelable {

public int id;
public String name;
public String src;
public int duration;
public String checksum;
public int type;
public Properties assetProperty;
public DoctorDirectory doctorDirectories;
public ConsultantDirectory consultantDirectory;

public Asset() {}

protected Asset(Parcel in) {
        id = in.readInt();
        name = in.readString();
        src = in.readString();
        duration = in.readInt();
        checksum = in.readString();
        type = in.readInt();
}

public static final Creator<Asset> CREATOR = new Creator<Asset>() {
        @Override
        public Asset createFromParcel(Parcel in) {
                return new Asset(in);
        }

        @Override
        public Asset[] newArray(int size) {
                return new Asset[size];
        }
};

@Override
public int describeContents() {
        return Parcelable.CONTENTS_FILE_DESCRIPTOR;
}

@Override
public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(src);
        parcel.writeInt(duration);
        parcel.writeString(checksum);
        parcel.writeInt(type);
}

@Override
public boolean equals(Object obj) {
        return super.equals(obj);
}

@Override
public int hashCode() {
        return super.hashCode();
}

}