package com.plustv.prosignsdk.models;

/**
 * Created by Rachit Solanki on 15/8/18.
 */
public class Properties {
//
//public String color ;
//public String fontFile ;
//public int fontSize;
//public int weatherType;
//public long rate;
//public String direction;
//public String mode;
//public String bg;
//public String unit;
//public String city;
//public String src;
//public long refresh;

    public int id;

    public String name;

    public String src;

    public int type;

    public String color; //10 rest font info

    public String fontFile;

    public int fontSize;//6 doctor personal info

    public int rate; //11  //slide timing

    public String direction;

    public String mode;

    public String bg;  //4  template background color

    public int refresh;

    public String city;

    public String unit;

    public String departmentHeaderColor; //9 background

    public String departmentNameColor; //8

    public String  doctorNameColor; //7

    public int departmentNameSize; // 5

    public int doctorImageSize; //1

    public int dictorImageBorder; //2

    public int doctorNameSize;  //3




}
