package com.plustv.prosignsdk.models;

import java.util.ArrayList;
import java.util.List;

public class Department {

    public int id;
    public String name;
    public List<DoctorDetails> doctorDetails = new ArrayList<>();


    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public List<DoctorDetails> getDoctorDetails() { return doctorDetails; }

    public void setDoctorDetails(List<DoctorDetails> doctorDetails) { this.doctorDetails = doctorDetails; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }




}
