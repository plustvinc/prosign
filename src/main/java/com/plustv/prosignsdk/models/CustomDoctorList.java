package com.plustv.prosignsdk.models;

import java.util.ArrayList;
import java.util.List;

public class CustomDoctorList {
    public int id;
    public String tabName;
    public DoctorDetails doctorDirectory;

    public List<CustomDepartment> departments = new ArrayList<>();



    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getTabName() { return tabName; }

    public void setTabName(String tabName) { this.tabName = tabName; }

    public DoctorDetails getDoctorDirectory() { return doctorDirectory; }

    public void setDoctorDirectory(DoctorDetails doctorDirectory) { this.doctorDirectory = doctorDirectory; }



}
