package com.plustv.prosignsdk;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Toast;

import com.plustv.prosignsdk.common.EventEmitter;
import com.plustv.prosignsdk.common.EventType;
import com.plustv.prosignsdk.common.FileUtility;
import com.plustv.prosignsdk.common.SyncStatusListener;
import com.plustv.prosignsdk.handler.FragmentTransactionHandler;
import com.plustv.prosignsdk.handler.PlaylistDispatcher;
import com.plustv.prosignsdk.handler.ScheduleWatcher;
import com.plustv.prosignsdk.models.Presentation;
import com.plustv.prosignsdk.models.Region;
import com.plustv.prosignsdk.models.RegionContainer;
import com.plustv.prosignsdk.models.Schedule;
import com.plustv.prosignsdk.models.SignageMessage;
import com.plustv.prosignsdk.rsync.Rsync;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author Rachit Solanki
 * @date 13/8/18
 */
public class SignageViewFragment extends Fragment implements EventEmitter{

FrameLayout signageFrameLayout;

Schedule schedule;
Presentation currentPresentation = null;

SyncStatusListener syncStatusListener;
Rsync rsync;

// Handlers
Handler playlistDispatcher;
Handler fragmentTransactionHandler;
Handler scheduleWatcher;


/**
 * Contains all presentations from single schedule
 */
Queue<Presentation> presentationQueue = new LinkedBlockingQueue<>();

/**
 *  <h1>Region Queue</h1>
 *  <p>
 *          Container to hold information about
 *          region associated with assets
 *          Container example
 *          [ regionId , Playlist with region assetProperty ]
 *  </p>
 *
 */
@SuppressLint("UseSparseArrays")
Map<Integer, RegionContainer> regionQueue = new HashMap<>();

private static String DEVICE_UUID;
private boolean playSignage = false;

/**
 * Creates instance of SignageViewFragment with required arguments
 * @param endPointApi to connect to with server
 */
@Deprecated
public static SignageViewFragment createInstance(String endPointApi) {
        SignageViewFragment signageViewFragment = new SignageViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("endPointApi",endPointApi);
        signageViewFragment.setArguments(bundle);
        return signageViewFragment;
}

public static SignageViewFragment createInstance() {
        return new SignageViewFragment();
}

public void init(String uuid) {

        if(uuid == null || uuid.isEmpty())
                throw new IllegalArgumentException("Device uuid cannot be null");

        this.DEVICE_UUID=uuid;
}

@Nullable
@Override
public View onCreateView(@NonNull LayoutInflater inflater,
                         @Nullable ViewGroup container,
                         @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.signage_main_layout,
                                      container,
                                      false);
        signageFrameLayout = view.findViewById(R.id.signageFrameLayout);
        return view;
}

@Override
public void onResume() {
        super.onResume();
        if(this.playSignage) {
                startSignage();
        }
}

@Override
public void onPause() {
        super.onPause();
        stopHandlers();
        rsync.stopRsync();
}

public void playSignage() {

        if(!this.playSignage)
                this.playSignage = true;

        /* Check if fragment is already resumed*/
        if(isResumed()) {
                startSignage();
        }
}


/**
 * To start signage.
 */
private void startSignage(){

        if( !isPermissionGranted() ) {
                return;
        }

        if(DEVICE_UUID == null || DEVICE_UUID.isEmpty()) {
                throw new RuntimeException("No uuid found please refer to the document on how to setup.");
        }

        if(signageFrameLayout == null) {
                throw new InflateException("Frame layout is not initialized in signage");
        }

        if(BuildConfig.DEBUG) {
                signageFrameLayout.setBackgroundColor(Color.GREEN);
        } else {
                signageFrameLayout.setBackgroundColor(Color.BLACK);
        }

        FileUtility.mainFolderInit();

        initHandlers();

        // Start watching and  downloading schedule
        scheduleWatcher =new ScheduleWatcher(this,DEVICE_UUID);
        scheduleWatcher.sendEmptyMessage(common_message);
        // Start content watching
        rsync = new Rsync(this);
        rsync.startRsync(getActivity());
}

private boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getActivity(),
                                "Read/Write storage permission\nrequired to run signage." ,
                                Toast.LENGTH_LONG).show();
                        return false;
                }

                if(getActivity().checkSelfPermission(Manifest.permission.INTERNET) !=
                        PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getActivity(),
                                "Internet Permission required" ,
                                Toast.LENGTH_LONG).show();
                        return false;
                }
        }
        return true;
}

/**
 * @param newPresentation Presentation popped from queue
 * refill map -> render regions -> start playing
 */
private void assignQueueToMap(Presentation newPresentation) {
        this.currentPresentation = newPresentation;
        this.regionQueue.clear();
        // Stop worker since we don't want to run them on,
        // Old presentation
        if(this.fragmentTransactionHandler !=null) {
                this.fragmentTransactionHandler.removeCallbacksAndMessages(null);
        }

        // Lets start to fill our map [ region -> asset queue ]
        for(int i = 0; i< this.currentPresentation.regions.size(); i++){
                Region region =  this.currentPresentation.regions.get(i);
                RegionContainer regionContainer = new RegionContainer( region.getRegionType(),
                                                                       new LinkedList<>(region.getPlaylist()
                                                                                              .getAssets()),
                                                                        region.getPlaylist().id,
                                                                        region.getPlaylist().name
                                                                      );
                this.regionQueue.put(region.getId(),regionContainer);
        }

        // re-render everything
        addRegionsToRoot(newPresentation.regions);

        //send messages to handle multiple regions
        for(Region region : newPresentation.regions) {
               this.fragmentTransactionHandler.sendEmptyMessage(region.getId());
        }

}

/**
 * Refill empty asset queue for specified region
 * @param regionId Region Id
 * @return RegionContainer from queue
 */
private RegionContainer refillEmptyQueue(int regionId) {
        if( this.regionQueue.containsKey(regionId) ) {
                for(Region region : this.currentPresentation.regions) {
                        if(region.getId() == regionId) {
                                this.regionQueue.get(regionId).assetQueue
                                                .addAll(region.getPlaylist().getAssets());
                                break;
                        }
                }
                return this.regionQueue.get(regionId);
        }

        return null;
}

final int common_message=1;
private void runSchedule(Schedule s) {
        // clear all
        // remove callbacks if any
        removeAllCallbacks();

        this.regionQueue.clear();
        this.presentationQueue.clear();
        this.currentPresentation = null;

        // refill queue
        if(s !=null) {
                this.presentationQueue.addAll(s.presentation);
        }

        // start handling presentation
        if(this.playlistDispatcher != null ) {
                if(s !=null) {
                        ((PlaylistDispatcher)this.playlistDispatcher)
                                        .setRepeatPresentation(schedule.repeatedPresentation);
                }
                this.playlistDispatcher.sendEmptyMessage(common_message);
        }
}

/**
 * @param syncStatusListener
 * To Listen Rsync events must set it before starting signage
 */
public void setSyncStatusListener(SyncStatusListener syncStatusListener){
        this.syncStatusListener = syncStatusListener;
}



/**
 *
 * Add Regions To Parent
 * @param regionList Regions to plot
 *
 */
private void addRegionsToRoot(@NonNull List<Region> regionList){
        notifyAllBaseFragmentRemoval();
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f,0.5f);
        alphaAnimation.setDuration(500);
        for(int i=0;i<signageFrameLayout.getChildCount();i++) {
                // set animation to child view about to remove
                signageFrameLayout.getChildAt(i).startAnimation(alphaAnimation);
        }

        signageFrameLayout.removeAllViews();
        signageFrameLayout.invalidate();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                /*
                 * There is no method for handling layer index on pre lollipop devices
                 * and TVs (including Phillips) so instead add them in ascending order
                 * to layout to get layer effect.
                 */
                Collections.sort(regionList, new Comparator<Region>() {
                        @Override
                        public int compare(Region region, Region t1) {
                                return region.getzIndex()-t1.getzIndex();
                        }
                });
        }
        for(Region region:regionList) {
                FrameLayout frameLayout=new FrameLayout(getActivity());
                LayoutParams params=new LayoutParams(region.getWidth(),region.getHeight());
                params.leftMargin= region.getLeft();
                params.topMargin=region.getTop();
                frameLayout.setLayoutParams(params);
                frameLayout.setBackgroundColor(0xff000000);
                frameLayout.setId(region.getId());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        frameLayout.setZ(region.getzIndex());
                }
                signageFrameLayout.addView(frameLayout);
        }
}

private void notifyAllBaseFragmentRemoval() {
        for(int i =0;i<signageFrameLayout.getChildCount();i++) {
                FrameLayout viewGroup = (FrameLayout) signageFrameLayout.getChildAt(i);
                if(getFragmentManager() != null) {
                        Fragment f= getFragmentManager().findFragmentById(viewGroup.getId());
                        if(f!=null) {
                                f.onDetach();
                        }
                }
        }
}

final String TAG = "SignageViewFragment";
@Override
public void emit(EventType eventType,Object payload) {

switch (eventType) {

        case PLAYLIST_COMPLETE:
                                  Log.e("Iam","herererer");
                                   if(payload instanceof Integer) {
                                           // Explicit casting to show return type of refillEmptyQueue
                                           payload = refillEmptyQueue((Integer) payload);
                                           Log.e("Iam","WHyNill "+(payload == null));
                                   }
                                   break;

        case ASSET_PLAYING: break;
        case SKIP_ASSET : break;
        case SKIP_PLAYLIST: break;

        case PRESENTATION_STARTED:
                                if(payload instanceof Presentation) {
                                        assignQueueToMap((Presentation) payload);
                                }
                                break;

        case SCHEDULE_CHANGED:  Log.d(TAG,"Schedule changed" );
                                if(payload instanceof Schedule) {
                                        schedule = (Schedule) payload;
                                        rsync.scheduleChange(schedule);
                                }

        case SCHEDULE_FINISHED:  Log.d(TAG,"Schedule Finished" );
                                 runSchedule(schedule);
                                 break;

        case SYNC_STARTED:
        case SYNC_FAILED:
        case SYNC_COMPLETE: break;
        default:break;
}

if(this.syncStatusListener !=null) {
        // Dispatch event to parent who registered itself to listen  events
        this.syncStatusListener.syncStatus(eventType,new SignageMessage(payload));
}

}

/**
 * Initialize all the handlers
 */
private void initHandlers() {
        playlistDispatcher = new PlaylistDispatcher(this, presentationQueue);
        fragmentTransactionHandler = new FragmentTransactionHandler(this,
                                                                     this.regionQueue,
                                                                     getFragmentManager());
}

/**
 * Remove all the handlers messages
 */
private void removeAllCallbacks() {
        if(playlistDispatcher !=null) {
                playlistDispatcher.removeCallbacksAndMessages(null);
        }
        if(fragmentTransactionHandler!=null) {
                fragmentTransactionHandler.removeCallbacksAndMessages(null);
        }

        if(scheduleWatcher!=null) {
                scheduleWatcher.removeCallbacksAndMessages(common_message);
        }

}

/**
 * Remove all messages and mark handlers as null.
 */
private void stopHandlers() {
        removeAllCallbacks();
        playlistDispatcher =null;
        fragmentTransactionHandler=null;
        scheduleWatcher = null;
        this.playSignage = false;
}

/**
 * Remove everything and stop signage
 * on detach from parent
 */
@Override
public void onDetach() {
        super.onDetach();
        // stopSignage();
        Log.d("SingageViewFragment","OnDetach");
}

}
