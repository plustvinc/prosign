package com.plustv.prosignsdk.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;

public class RoundedCornerLayout extends LinearLayout {

    private static final float CORNER_RADIUS = 10.0f;
    public static final long serialVersionUID = 0;
    private float cornerRadius;
    private Bitmap maskBitmap;
    private Paint maskPaint;
    private Paint paint;



    private Bitmap createMask(int width, int height) {

        Bitmap mask = Bitmap.createBitmap(width, height, Config.ALPHA_8);
        Canvas canvas = new Canvas(mask);
        Paint paint = new Paint(1);
        paint.setColor(-1);
        canvas.drawRect(0.0f, 0.0f, (float) width, (float) height, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) width, (float) height), this.cornerRadius, this.cornerRadius, paint);
        return mask;
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {


        this.cornerRadius = TypedValue.applyDimension(1, CORNER_RADIUS, context.getResources().getDisplayMetrics());
        this.paint = new Paint(1);
        this.maskPaint = new Paint(3);
        this.maskPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        setWillNotDraw(false);
    }

    public void draw(Canvas canvas) {

        Bitmap offscreenBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Config.ARGB_8888);
        Canvas offscreenCanvas = new Canvas(offscreenBitmap);
        super.draw(offscreenCanvas);
        this.maskBitmap = createMask(canvas.getWidth(), canvas.getHeight());
        offscreenCanvas.drawBitmap(this.maskBitmap, 0.0f, 0.0f, this.maskPaint);
        canvas.drawBitmap(offscreenBitmap, 0.0f, 0.0f, this.paint);
    }

    public RoundedCornerLayout(Context context) {


        super(context);
        init(context, null, 0);
    }

    public RoundedCornerLayout(Context context, AttributeSet attrs) {


        super(context, attrs);
        init(context, attrs, 0);
    }

    public RoundedCornerLayout(Context context, AttributeSet attrs, int defStyle) {


        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }
}
