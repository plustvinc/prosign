package com.plustv.prosignsdk.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.format.DateFormat;
import android.util.AttributeSet;

import java.util.Calendar;


public class DigitalClock extends android.support.v7.widget.AppCompatTextView
{
    private String TAG =DigitalClock.class.getName();

    public DigitalClock(Context context) {
        super(context);
        createTime();
    }

    public DigitalClock(Context context, AttributeSet attrs) {
        super(context, attrs);
        createTime();
    }

    public DigitalClock(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createTime();
    }



   @Override
    protected void onAttachedToWindow(){
        super.onAttachedToWindow();
        registerReceiver();
    }

    @Override
    protected void onDetachedFromWindow(){
        super.onDetachedFromWindow();
        unregisterReceiver();
    }

    private void registerReceiver() {
        final IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        getContext().registerReceiver(mIntentReceiver, filter);
    }

    private void unregisterReceiver()
    {
        getContext().unregisterReceiver(mIntentReceiver);
    }

    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onTimeChanged();
        }
    };

    private Calendar mTime;
    final CharSequence mFormat="h:mm a";

    private void createTime() {
            mTime = Calendar.getInstance();
            onTimeChanged();
    }

    private void onTimeChanged() {
        mTime.setTimeInMillis(System.currentTimeMillis());
        setText(DateFormat.format(mFormat, mTime));
    }



}