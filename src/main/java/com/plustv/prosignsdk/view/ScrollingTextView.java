package com.plustv.prosignsdk.view;

import android.content.Context;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.Scroller;

public class ScrollingTextView extends android.support.v7.widget.AppCompatTextView {

    private boolean mPaused;
    private long mRndDuration;
    private Scroller mSlr;
    private int mXPaused;
    public static final int RIGHT = -1;
    public static final int LEFT = 1;
    private int direction;

    private int calculateScrollingLen() {
         TextPaint tp = getPaint();
        Rect rect = new Rect();
        String strTxt = getText().toString();
        tp.getTextBounds(strTxt, 0, strTxt.length(), rect);
        return rect.width() + getWidth();
    }

    public void computeScroll() {
         super.computeScroll();
        if (this.mSlr != null && this.mSlr.isFinished() && !this.mPaused) {
            startScroll();
        }
    }

    public long getRndDuration() {
             return this.mRndDuration;
      }

    public boolean isPaused() {
             return this.mPaused;
     }

    public void pauseScroll() {
       if (this.mSlr != null && !this.mPaused) {
            this.mPaused = true;
            this.mXPaused = this.mSlr.getCurrX();
            this.mSlr.abortAnimation();
        }
    }

    public void resumeScroll() {
       if (this.mPaused) {
            setHorizontallyScrolling(true);
            this.mSlr = new Scroller(getContext(), new LinearInterpolator());
            setScroller(this.mSlr);
            int scrollingLen = calculateScrollingLen();
            int distance = scrollingLen - (getWidth() + this.mXPaused);
            int duration = new Double((((double) (this.mRndDuration * distance)) * 1.0d) / ((double) scrollingLen)).intValue();
            setVisibility(0);
            this.mSlr.startScroll(this.mXPaused,0,direction*distance,0,duration);
            invalidate();
            this.mPaused = false;
        }
    }

    public void setRndDuration(long duration) {

        this.mRndDuration = duration;
    }

    public void startScroll() {

        this.mXPaused = getWidth() * -1;
        this.mPaused = true;
        resumeScroll();
    }

    public ScrollingTextView(Context context) {
        this(context, null);
        setSingleLine();
        setEllipsize(null);
        setVisibility(4);
    }

    public void setDirection(int direction){
        this.direction=direction;
    }

    public ScrollingTextView(Context context, AttributeSet attrs) {

        this(context, attrs, 16842884);
        direction=LEFT;
        setSingleLine();
        setEllipsize(null);
        setVisibility(4);
    }

    public ScrollingTextView(Context context, AttributeSet attrs, int defStyle) {
         super(context, attrs, defStyle);
        direction=LEFT;
        this.mRndDuration = 30000;
        this.mXPaused = 0;
        this.mPaused = true;
        setSingleLine();
        setEllipsize(null);
        setVisibility(4);
    }
}
