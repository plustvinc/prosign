package com.plustv.prosignsdk.rsync;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;

import com.plustv.prosignsdk.common.EventEmitter;
import com.plustv.prosignsdk.common.EventType;
import com.plustv.prosignsdk.common.FileUtility;
import com.plustv.prosignsdk.models.Asset;
import com.plustv.prosignsdk.models.Schedule;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.DownloadManager.ACTION_DOWNLOAD_COMPLETE;
import static android.app.DownloadManager.COLUMN_REASON;
import static android.app.DownloadManager.COLUMN_STATUS;
import static android.app.DownloadManager.EXTRA_DOWNLOAD_ID;
import static android.app.DownloadManager.Query;
import static android.app.DownloadManager.Request;
import static android.app.DownloadManager.STATUS_SUCCESSFUL;
import static android.content.Context.DOWNLOAD_SERVICE;
import static com.plustv.prosignsdk.common.FileUtility.decodeFileName;
import static com.plustv.prosignsdk.common.FileUtility.getRelativePath;

/**
 * Created by Rachit Solanki on 13/8/18.
 *
 * Helper class to download and manage crons.
 *
 */
public class Rsync implements ContentSyncNotify {

private DownloadManager dm;
private AssetSanityMaintainer assetSanityMaintainer;
@SuppressLint("UseSparseArrays")
private HashMap<Long, Asset> assetHashMap = new HashMap<>();
private List<Asset> totalAssetList = new ArrayList<>();
private boolean isDownloadingInProgress = false;
private HandlerThread assetHandlerThread;
private EventEmitter eventEmitter;
private Context context;

public Rsync(EventEmitter eventEmitter) {
        this.eventEmitter = eventEmitter;
        Log.e("Rsync created","rsync created");
}

public void startRsync( @NonNull Context context) {
        assetHandlerThread = new HandlerThread("HandlerThread");

        this.context = context;
        assetHandlerThread.start();
        assetSanityMaintainer = new AssetSanityMaintainer(assetHandlerThread.getLooper(),
                                                          totalAssetList,
                                                          this);
        assetSanityMaintainer.sendEmptyMessage(1);

        dm = (DownloadManager) this.context.getSystemService(DOWNLOAD_SERVICE);
        this.context.registerReceiver( rsyncService,
                                       new IntentFilter(ACTION_DOWNLOAD_COMPLETE)
                                      );
}

/**
 * This Function call will request to download.
 * Check {@link AssetSanityMaintainer}
 * @param schedule Scheduled content to download
 */
public void scheduleChange(Schedule schedule) {
        if(schedule == null) {
                return;
        }

        totalAssetList.clear();
        totalAssetList.addAll(schedule.assetToDownload);
        this.assetSanityMaintainer.assetListUpdated(schedule.assetToDownload);
        // TODO remove all pending downloads of previous schedule
        // Right now it will download and leave this task on assetSanityManager to clear unused files,
        // But will consume unnecessary data in order to download content which is no longer in use.
        //isDownloadingInProgress = false;
        /*
         if(isDownloadingInProgress) {
                dm.remove();
         }
        */
}

@Override
public void downloadContent(List<Asset> assetList) {
        // don't accept until we have downloaded all
        if(isDownloadingInProgress) {
                return;
        }

        /*
         * Unexpected behaviour may occur if single asset added.
         * multiple times
         */
        for(Asset asset : assetList) {
                Log.e("asset src ","asset src "+ asset.src);
                FileUtility.createFileLock(asset.name);
                Request request = new Request(Uri.parse(asset.src));
                request.setDestinationInExternalPublicDir( getRelativePath(),
                                                           decodeFileName(asset.src));
                assetHashMap.put(dm.enqueue(request), asset);
        }

        if(assetList.size()>0)
                isDownloadingInProgress = true;

        assetList.clear();

}

public void stopRsync() {
        try{
                // If stop rsync called multiple times.
                this.context.unregisterReceiver(rsyncService);
        }catch (IllegalArgumentException  e) {
                e.printStackTrace();
        }
        if(assetSanityMaintainer!=null) {
                assetSanityMaintainer.removeCallbacksAndMessages(null);
        }

        if(assetHandlerThread!=null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        assetHandlerThread.quitSafely();
                } else {
                        assetHandlerThread.quit();
                }
        }
        dm=null;
}

public final BroadcastReceiver rsyncService = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                //Toast.makeText(context,"Asset complete with action "+action,Toast.LENGTH_SHORT).show();
                if (ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                        long downloadId = intent.getLongExtra(EXTRA_DOWNLOAD_ID,
                                                              0);


                        if(assetHashMap.containsKey(downloadId)) {
                                Asset a = assetHashMap.get(downloadId);
                                FileUtility.removeLock(a.name);
                                if(isVideo(a)) {

                                        // TODO : send some boradcast
                                        // FIXME : DON'T use this
                                        EventBus.getDefault().post(a.name);
                                }
                                assetHashMap.remove(downloadId);

                        }

                        if(assetHashMap.containsKey(downloadId)) {
                                Asset a = assetHashMap.get(downloadId);
                                FileUtility.removeLock(a.name);
                                if(isHtml(a)) {

                                        // TODO : send some boradcast
                                        // FIXME : DON'T use this
                                        EventBus.getDefault().post(a.name);
                                }
                                assetHashMap.remove(downloadId);

                        }



                        if(assetHashMap.containsKey(downloadId)) {
                                Asset a = assetHashMap.get(downloadId);
                                FileUtility.removeLock(a.name);
                                if(isPdf(a)) {

                                        // TODO : send some boradcast
                                        // FIXME : DON'T use this
                                        EventBus.getDefault().post(a.name);
                                }
                                assetHashMap.remove(downloadId);

                        }

				if(assetHashMap.containsKey(downloadId)) {
					  Asset a = assetHashMap.get(downloadId);
					  FileUtility.removeLock(a.name);
					  if(isFm(a)) {
						    // TODO : send some boradcast
						    // FIXME : DON'T use this
						    EventBus.getDefault().post(a.name);
					  }
					  assetHashMap.remove(downloadId);

				}



                        if(assetHashMap.size() == 0 ) {
                                Rsync.this.isDownloadingInProgress = false;
                                Rsync.this.eventEmitter.emit( EventType.SYNC_COMPLETE,
                                                              null);
                        }
                }

        }

};



/**
 * Check if download was valid, see issue
 * http://code.google.com/p/android/issues/detail?id=18462
 * @param downloadId download id
 * @return if download is successful
 */
@Deprecated
private boolean validDownload(long downloadId) {


        //Verify if download is a success
        Cursor c= dm.query(new Query().setFilterById(downloadId));

        if(c.moveToFirst()){
                int status = c.getInt(c.getColumnIndex(COLUMN_STATUS));
                //Download is valid, celebrate
                if(status == STATUS_SUCCESSFUL){
                        return true;
                }else{
                        int reason = c.getInt(c.getColumnIndex(COLUMN_REASON));
                        return false;
                }
        }
        return false;
}


public boolean isVideo(Asset asset) { return asset.name.endsWith(".mp4"); }

public boolean isHtml(Asset asset) { return asset.name.endsWith(".html"); }

public boolean isPdf(Asset asset) { return asset.name.endsWith(".pdf"); }

public boolean isFm(Asset asset) {
	  return asset.name.endsWith(".mp3");
}




}
