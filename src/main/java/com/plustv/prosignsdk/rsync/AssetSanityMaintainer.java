package com.plustv.prosignsdk.rsync;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;

import com.plustv.prosignsdk.common.FileUtility;
import com.plustv.prosignsdk.common.FragmentPoolManager;
import com.plustv.prosignsdk.common.MD5;
import com.plustv.prosignsdk.models.Asset;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static com.plustv.prosignsdk.common.DefaultSchedule.DEFAULT_ASSET_VIDEO;
import static com.plustv.prosignsdk.common.FileUtility.SCHEDULE_JSON;
import static com.plustv.prosignsdk.common.FileUtility.getLockExtension;

/**
 * Created by Rachit Solanki on 17/8/18.
 *
 * AssetSanityMaintainer : cron job to check assets integrity,
 * and if new content is available feed to downloader.
 *
 */
class AssetSanityMaintainer extends Handler {

private ContentSyncNotify contentSyncNotify;
/**
 * Holds valid assets which are required to play by
 * application.
 */
private ConcurrentHashMap<String,Asset> assetHashMap = new ConcurrentHashMap<>();
/**
 * Assets to download and feed to download manager in rsync
 */
private ArrayList<Asset> assetToDownload = new ArrayList<>();
private File contentDirectory;
private boolean init;

AssetSanityMaintainer(Looper looper, List<Asset> assetList,
                      ContentSyncNotify contentSyncNotify) {
        super(looper);
        this.contentSyncNotify=contentSyncNotify;
        init = true;
        contentDirectory = new File(FileUtility.getAbsolutePath());

        // delete all the files locks,
        // if application quits and then restarts.
        for(File file : contentDirectory.listFiles()) {
               if(file.getName().endsWith(FileUtility.getLockExtension())) {
                       file.delete();
               }
        }
}

@Override
public void handleMessage(Message msg) {
        super.handleMessage(msg);

        Set<Asset> set = new HashSet<>();
        // Download if file not exists
        for(String key : assetHashMap.keySet()) {

                Asset a = assetHashMap.get(key);
                // download if file do not exists


                boolean download = false;


                /*
                 * File exists check the following conditions.
                 * reset download flag and check if checksum is mismatched,
                 * Check if asset is actually real and exists on physical disk,
                 * and file lock do not exists for it.
                 */
                if(isAssetReal(a.type)) {
                        download =  FileUtility.fileExists(key)?
                                        !MD5.checkMD5(a.checksum, FileUtility.getFile(key)):
                                        !FileUtility.isLockExists(key);

                }

                /*
                * 1) Should not already on download list
                * 2) download condition is true
                * 3) Must not be default_page video since it won't exist on cloud
                */
                if( !set.contains(a) &&
                    download &&
                    !a.src.equals(DEFAULT_ASSET_VIDEO) ) {
                        set.add(a);
                }





        }

        // remove unnecessary files
        if(contentDirectory!=null && contentDirectory.listFiles() !=null) {
                for(File file : contentDirectory.listFiles()) {
                        // exclude schedule.json
                        if( SCHEDULE_JSON.equals(file.getName()) ) {
                                continue;
                        }
                        if(!init && !assetHashMap.containsKey(file.getName()) ) {
                                /*
                                 * If file is a lock file check weather it is stale lock
                                 * or lock of downloading file
                                 * because if application crashes in between downloading
                                 * it lock will remains in folder
                                 */
                                if(FileUtility.isFileALock(file.getName()) ) {
                                        // we need to check if original file of this lock is in hash map
                                        int stringLength  = file.getName().length()- getLockExtension().length();
                                        String assetName =  file.getName().substring(0,stringLength);
                                        if(assetHashMap.containsKey(assetName)) {
                                                // no need to do anything
                                                continue;
                                        }

                                }

                                // @ignore result
                                file.delete();

                        }
                }




        }

        // clear previous result
        assetToDownload.clear();
        // Add all set
        assetToDownload.addAll(set);
        if(assetToDownload.size()>0) {
                this.contentSyncNotify.downloadContent(assetToDownload);
        }

        sendEmptyMessageDelayed(msg.what, 20 * 1000);
}

/**
 * Notify when asset list got update
 */
public void assetListUpdated(@NonNull List<Asset> assetList) {
        if(init) {
                init = false;
        }
        assetHashMap.clear();
        for(Asset asset : assetList) {
                assetHashMap.put(asset.name,asset);
        }

}


private boolean isAssetReal(int assetType) {
        switch (assetType) {
                case FragmentPoolManager.IMAGE :
                case FragmentPoolManager.VIDEO :
                case FragmentPoolManager.EXCEL :
                case FragmentPoolManager.PDF :
                case FragmentPoolManager.AUDIO :
                case FragmentPoolManager.PPT :


                                                         return true;
                default:return false;
        }

}


}