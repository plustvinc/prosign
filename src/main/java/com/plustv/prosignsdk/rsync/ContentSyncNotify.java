package com.plustv.prosignsdk.rsync;

import com.plustv.prosignsdk.models.Asset;

import java.util.List;

/**
 * Created by Rachit Solanki on 17/8/18.
 * callback to provide list of files to download
 */
interface ContentSyncNotify  {
        void downloadContent(List<Asset> assetList);
}
