package com.plustv.prosignsdk.common;

import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Rachit Solanki on 13/8/18.
 */
public class FileUtility {

private static final String CONTENT_FOLDER =".AndroidDs";
public static final String SCHEDULE_JSON ="schedule.json";
private static final String LOCK_EXTENSION =".lock";
private static String BASE_PATH = Environment.getExternalStorageDirectory().getPath()+
                                 File.separator+CONTENT_FOLDER;


public static void changeBasePath(@NonNull String newPath) {
        BASE_PATH = newPath;
        if(!new File(BASE_PATH).exists()){
                new File(BASE_PATH).mkdirs();
        }
}

public static String getAbsolutePath() {
        return BASE_PATH;
}

public static String getRelativePath() {
        return CONTENT_FOLDER;
}

public static String getScheduleName() {
        return  BASE_PATH+File.separator+SCHEDULE_JSON;
}

public static String decodeFileName(String path){
        if(path == null) {
                return "";
        }
        Uri uri = Uri.parse(path);
        return uri.getLastPathSegment();
}

/**
 *
 */
public static void mainFolderInit() {
        File f = new File(BASE_PATH);
        if(!f.exists()) {
                f.mkdirs();
        }
}

@NonNull
public static File getFile(String fileName) {
        if(!new File(BASE_PATH).exists()){
                new File(BASE_PATH).mkdirs();
        }

        return new File(BASE_PATH, fileName);
}

public static boolean fileExists(String fileName) {
        return new File(BASE_PATH,fileName).exists();
}


@NonNull
public static String fileToString(File file) throws IOException {
        return convertStreamToString(new FileInputStream(file));
}

public static void stringToFile(String data,File fileToWrite) throws IOException {
        stringToFile(data,fileToWrite,false);
}

public static void stringToFile(String data,File fileToWrite,boolean append) throws IOException {
        FileOutputStream fileOutputStream=new FileOutputStream(fileToWrite,append);
        fileOutputStream.write(data.getBytes("UTF-8"));
        fileOutputStream.close();
}


@NonNull
public static String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
}

/**
 * All file locks are in similar fashion <br/>
 * [fileName].lock<br/>
 * For example<br/>
 * file.mp4.lock<br/>
 *
 */
public static void createFileLock(String fileName) {
        try {
                stringToFile("lock",getFile(fileName+LOCK_EXTENSION));
        } catch (IOException e) {
                e.printStackTrace();
                /* discard silently its not fatal if no lock was created */
        }
}

/**
 * @param name of file
 * @return true if file is having lock extension.
 */
public static boolean isFileALock(@Nullable String name) {
        return name != null && !"".equals(name) && name.endsWith(LOCK_EXTENSION);

}

/**
 * Is file lock exists
 */
public static boolean isLockExists(String fileName) {
        return fileExists(fileName+LOCK_EXTENSION);
}

/**
 * Remove lock if file exists
 */
public static void removeLock(String fileName) {
        File f = getFile(fileName + LOCK_EXTENSION);
        if (f.exists()) {
                // ignore result
                f.delete();
        }

}

public static String getLockExtension() {
        return LOCK_EXTENSION;
}

}

