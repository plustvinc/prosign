package com.plustv.prosignsdk.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.plustv.prosignsdk.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NetworkFactory {

    public static String BASE_URL;



    private static OkHttpClient okHttpClientLong;

    public static Gson GSON = new GsonBuilder()
                                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                                    .create();

    //http://prosign.plustv.live:9090/
    //http://45.55.239.12:9090/
    static {
        if( BuildConfig.DEBUG ) {
            BASE_URL = "http://192.168.1.14:9000/";
        } else {
            BASE_URL = "http://192.168.1.14:9000/";
        }
        okHttpClientLong = new OkHttpClient.Builder().connectTimeout(2, TimeUnit.MINUTES).build();
    }

    public static <T> T createNetworkInterface(Class<T> interfaceClass) {

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClientLong)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(GSON))
                .build();

        return retrofit.create(interfaceClass);
    }
}
