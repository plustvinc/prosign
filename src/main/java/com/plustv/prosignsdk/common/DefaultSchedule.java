package com.plustv.prosignsdk.common;

import com.plustv.prosignsdk.models.Asset;
import com.plustv.prosignsdk.models.Playlist;
import com.plustv.prosignsdk.models.Presentation;
import com.plustv.prosignsdk.models.Region;
import com.plustv.prosignsdk.models.Schedule;

import java.util.ArrayList;

import static com.plustv.prosignsdk.common.FragmentPoolManager.VIDEO;

/**
 * Created by Rachit Solanki on 3/10/18.
 */
public class DefaultSchedule {

private static final Schedule SCHEDULE;
public static final String DEFAULT_ASSET_VIDEO = "android.res.prosigncloud";

static {
        SCHEDULE = new Schedule();
        SCHEDULE.md5checksum = "mfmaspodfigughbfjd-3432213433";
        SCHEDULE.assetToDownload = new ArrayList<>();
        SCHEDULE.name = "default_page";
        // Presentation
        Presentation  presentation = new Presentation();
        presentation.duration = 30;
        presentation.name = "ProSignCloud";
        presentation.regions = new ArrayList<>();
        presentation.regions.add(getRegion_l_1920());
        presentation.startTime = "";
        presentation.endTime = "";

        SCHEDULE.presentation = new ArrayList<>();
        SCHEDULE.presentation.add(presentation);
}

private static Region getRegion_l_1920() {
        Region region = new Region();
        region.setRegionType(RegionType.DYNAMIC);
        region.setColour("#FF000000");
        region.setzIndex(0);
        region.setId(1);
        region.setWidth(1920);
        region.setHeight(1080);
        region.setPlaylist(getPlaylist());
        return region;
}

private static Playlist getPlaylist() {
        Playlist playlist = new Playlist();
        playlist.name = "ProSignCloud";
        playlist.duration = 30;
        playlist.id = 100;
        playlist.assets = getAssetList();
        return playlist;
}

private static ArrayList<Asset> getAssetList() {
        ArrayList<Asset> assets = new ArrayList<>();
        Asset asset = new Asset();
        asset.duration = 30;
        asset.type = VIDEO;
        /* Asset has no source */
        asset.name = "default_page.mp4";
        asset.src = DEFAULT_ASSET_VIDEO;
        asset.checksum = "hsknxcvoupr23234nkhjkfsdnhujs";
        assets.add(asset);
        return assets;
}

private static Region getRegion_P_1920() {
        Region region = new Region();
        region.setRegionType(RegionType.DYNAMIC);
        region.setColour("#FF000000");
        region.setzIndex(0);
        region.setId(1);
        region.setWidth(1080);
        region.setHeight(1920);
        region.setPlaylist(getPlaylist());
        return region;
}

private static Region getRegion_L_1280() {
        Region region = new Region();
        region.setRegionType(RegionType.DYNAMIC);
        region.setColour("#FF000000");
        region.setzIndex(0);
        region.setId(1);
        region.setWidth(1280);
        region.setHeight(720);
        region.setPlaylist(getPlaylist());
        return region;
}

private static Region getRegion_P_1280() {
        Region region = new Region();
        region.setRegionType(RegionType.DYNAMIC);
        region.setColour("#FF000000");
        region.setzIndex(0);
        region.setId(1);
        region.setWidth(720);
        region.setHeight(1280);
        region.setPlaylist(getPlaylist());
        return region;
}

public static Schedule getDefaultSchedule() {
        return SCHEDULE;
}

}
