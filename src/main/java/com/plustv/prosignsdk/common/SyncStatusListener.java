package com.plustv.prosignsdk.common;

import com.plustv.prosignsdk.models.SignageMessage;

/**
 * Created by Rachit Solanki on 13/8/18.
 * Callback to expose events to outside world
 */
public interface SyncStatusListener {
        void syncStatus(EventType event, SignageMessage signageMessage);
}
