package com.plustv.prosignsdk.common;


import android.support.v4.app.FragmentTransaction;

import com.plustv.prosignsdk.R;


public class AnimationFactory {

    private static AnimationFactory INSTANCE;
    public static final long serialVersionUID = 0;

    private final int  rightToLeft=1;
    private final int  LeftToRight=2;
    private final int  wipeTop=3;
    private final int  wipeBottom=4;
    private final int  fadeIn=5;

    public static AnimationFactory getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new AnimationFactory();
        }
        return INSTANCE;
    }

    public void setAnimation(int animationType, FragmentTransaction ft) {
         switch (animationType) {

            case rightToLeft:
                    ft.setCustomAnimations(R.anim.swipe_from_right, R.anim.swipe_to_left);
                  break;

            case LeftToRight:
                    ft.setCustomAnimations(R.anim.swipe_from_left, R.anim.swipe_to_right);
                 break;

            case wipeTop:
                    ft.setCustomAnimations(R.anim.swipe_from_bottom, R.anim.swipe_to_top);
                 break;

            case wipeBottom:
                    ft.setCustomAnimations(R.anim.swipe_from_top, R.anim.swipe_to_bottom);
                break;

            case fadeIn:
                    ft.setCustomAnimations(R.anim.fade_in_anim , R.anim.fade_out_anim);
                 break;

            default:
                    ft.setCustomAnimations(R.anim.fade_in_anim , R.anim.fade_out_anim);
                 break;


         }
    }

}
