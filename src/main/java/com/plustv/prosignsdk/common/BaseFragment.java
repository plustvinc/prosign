package com.plustv.prosignsdk.common;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plustv.prosignsdk.models.Asset;

/**
 * Created by Rachit Solanki on 13/8/18.
 */
public abstract class BaseFragment extends Fragment {

public abstract int getLayout();
public abstract void onCreateView(View view);

public View view;
public Asset asset;

@Nullable
@Override
public View onCreateView(@NonNull LayoutInflater inflater,
                         @Nullable ViewGroup container,
                         @Nullable Bundle savedInstanceState) {

        if (view == null) {
                view = inflater.inflate(getLayout(), container, false);
                onCreateView(view);
        }


        return view;
}

public void setAsset(Asset asset) {
        this.asset = asset;
}


/**
 * @param colorCode Accepts 6 character hex code for color
 * @return color int value
 */
protected int parseColor(String colorCode) {

        if("transparent".equalsIgnoreCase(colorCode)) {
                return Color.TRANSPARENT;
        }

        try {
                /* prepend alpha */
                return Color.parseColor("#FF" + colorCode);
        }catch (IllegalArgumentException e) {
                return Color.WHITE;
        }
}

protected float getScreenDensity() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        return metrics.density;
}

@Override
public void onDetach() {
        super.onDetach();
}

}
