package com.plustv.prosignsdk.common;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by plusTv on 11/2/17.
 */

public class RssParser {


    public static String rssFeed(InputStream inputStream) throws XmlPullParserException, IOException {

        String title = null;
        String link = null;
        String description = null;
        boolean isItem = false;

        String rssFeed="||";

        XmlPullParser xmlPullParser = Xml.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        xmlPullParser.setInput(inputStream, null);



        xmlPullParser.nextTag();
        while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
            int eventType = xmlPullParser.getEventType();

            String name = xmlPullParser.getName();
            if(name == null)
                continue;

            if(eventType == XmlPullParser.END_TAG) {
                if(name.equalsIgnoreCase("item")) {
                    isItem = false;
                }
                continue;
            }

            if (eventType == XmlPullParser.START_TAG) {
                if(name.equalsIgnoreCase("item")) {
                    isItem = true;
                    continue;
                }
            }

            String result= "";

            if (xmlPullParser.next() == XmlPullParser.TEXT) {
                result = xmlPullParser.getText();
                xmlPullParser.nextTag();
            }

            if (name.equalsIgnoreCase("title")) {
                title = result;
            } else if (name.equalsIgnoreCase("link")) {
                link = result;
            } else if (name.equalsIgnoreCase("description")) {
                description = result;
            }

            if (title != null && link != null && description != null) {
                if(isItem) {
                    rssFeed+=" "+title;
                    rssFeed+=" ||";
                }

                title = null;
                link = null;
                description = null;
                isItem = false;
            }

        }


        inputStream.close();

        return rssFeed;
    }



}


