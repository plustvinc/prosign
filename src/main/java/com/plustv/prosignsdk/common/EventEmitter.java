package com.plustv.prosignsdk.common;

/**
 * Created by Rachit Solanki on 13/8/18.
 */
public interface EventEmitter {
        /**
         * @param eventType Type of event to dispatch
         * @param payload Generic payload
         */
        void emit(EventType eventType,Object payload);
}
