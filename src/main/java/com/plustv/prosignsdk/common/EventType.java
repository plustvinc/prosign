package com.plustv.prosignsdk.common;

/**
 * Created by Rachit Solanki on 13/8/18.
 * Event types for event emitter
 */
public enum EventType {
        /* All assets finished*/
        PLAYLIST_COMPLETE,
        /* Assets playing*/
        ASSET_PLAYING,
        /* Presentation Started */
        PRESENTATION_STARTED,
        /* Scheduled finished*/
        SCHEDULE_FINISHED,
        /* Skip asset*/
        SKIP_ASSET,
        /* Skip playlist */
        SKIP_PLAYLIST,
        /* Schedule changed */
        SCHEDULE_CHANGED,
        /* Sync started */
        SYNC_STARTED,
        /* Sync complete */
        SYNC_COMPLETE,
        /* Sync failed */
        SYNC_FAILED
}
