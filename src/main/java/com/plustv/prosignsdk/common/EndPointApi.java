package com.plustv.prosignsdk.common;

import com.plustv.prosignsdk.models.MappedSchedule;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Rachit Solanki on 18/8/18.
 */
public interface EndPointApi {
        @Deprecated
        @GET
        Call<MappedSchedule> getScheduleJson(@Url String endPointApi);

        @GET("api/v1/schedule-to-play")
        Call<MappedSchedule> getScheduleJsonByUUID(@Query("uuid") String deviceUUID);


        @GET
        Call<ResponseBody> yahooWeather(@Url String url);
}
