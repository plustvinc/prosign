package com.plustv.prosignsdk.common;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rachit Solanki on 13/8/18.
 */
public enum RegionType {
        /* Static region */
        @SerializedName("0")
        STATIC,
        /* Static region */
        @SerializedName("1")
        STATIC_LIVE,
        /* Dynamic region */
        @SerializedName("2")
        DYNAMIC
}
