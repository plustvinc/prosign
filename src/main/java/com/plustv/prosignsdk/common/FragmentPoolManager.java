package com.plustv.prosignsdk.common;


import android.os.Build;

import com.plustv.prosignsdk.fragments.ClockFragment;
import com.plustv.prosignsdk.fragments.ConsultantFragment;
import com.plustv.prosignsdk.fragments.DirectoryFragment;
import com.plustv.prosignsdk.fragments.PptExoPlayerFragment;
import com.plustv.prosignsdk.fragments.ExcelFragment;
import com.plustv.prosignsdk.fragments.PptLiveMediaPlayerFragment;
import com.plustv.prosignsdk.fragments.ExoPlayerFragment;
import com.plustv.prosignsdk.fragments.AudioFragment;
import com.plustv.prosignsdk.fragments.RadioFragment;
import com.plustv.prosignsdk.fragments.ImageFragment;
import com.plustv.prosignsdk.fragments.PdfFragment;
import com.plustv.prosignsdk.fragments.RssFragment;
import com.plustv.prosignsdk.fragments.ScrollTextFragment;
import com.plustv.prosignsdk.fragments.LiveMediaPlayerFragment;
import com.plustv.prosignsdk.fragments.WeatherFragment;
import com.plustv.prosignsdk.fragments.WebViewFragment;

public class FragmentPoolManager {

        public static final int IMAGE = 1;
        public static final int VIDEO = 2;
        public static final int SCROLL_TEXT = 3;
        public static final int WEB_VIEW = 4;
        public static final int WEATHER_FORECAST = 5;
        public static final int CLOCK = 6;
        public static final int RSS = 7;
        public static final int STATIC_TEXT = 8;
        public static final int LIVE_STREAMING = 9;
        public static final int DOCTOR_DIRECTORY = 10;
        public static final int CONSULTANT_DIRECTORY = 11;
        public static final int PPT = 12;
        public static final int EXCEL = 13;
        public static final int PDF = 14;
        public static final int AUDIO = 16;
        public static final int RADIO = 17;
        public static final String EXO = "exo";
        public static final String NATIVE = "native";
        // Exo player min support is KitKat
        public static String VIDEO_PLAYER = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)?
                                                                                             EXO:
                                                                                             NATIVE;



        public static final String MODE = "background" ;

    public static int IMAGE_FRAGMENT;
    public static int GIF_VIEW_FRAGMENT;
    public static int VIDEO_FRAGMENT;
    public static int SCROLL_TEXT_FRAGMENT;
    public static int RSS_FRAGMENT;
    public static int WEB_VIEW_FRAGMENT;
    public static int CLOCK_FRAGMENT;
    private static FragmentPoolManager instance;


        static {
                instance = new FragmentPoolManager();
        }

        private FragmentPoolManager() {

        }

        public static FragmentPoolManager getInstance() {
                return instance;
        }

        public BaseFragment getFragmentFor(int mediaType) {

                BaseFragment baseFragment = null;
                switch (mediaType) {

                        case IMAGE:
                                baseFragment = new ImageFragment();
                                break;

                        case LIVE_STREAMING:
                                baseFragment =new LiveMediaPlayerFragment();
                                break;


                        case VIDEO:
                                if(VIDEO_PLAYER.equals(NATIVE)) {
                                        baseFragment = new LiveMediaPlayerFragment();
                                } else {
                                        baseFragment = new ExoPlayerFragment();
                                }
                                break;

                        case STATIC_TEXT :
                        case SCROLL_TEXT :
                                baseFragment = new ScrollTextFragment();
                                break;

                        case WEB_VIEW:
                                baseFragment = new WebViewFragment();
                                break;

                        case CLOCK:
                                baseFragment = new ClockFragment();
                                break;

                        case RSS:
                                baseFragment = new RssFragment();
                                break;

                        case WEATHER_FORECAST :
                                baseFragment = new WeatherFragment();
                                break;

                        case DOCTOR_DIRECTORY :
                                baseFragment = new DirectoryFragment();
                                break;

                        case CONSULTANT_DIRECTORY :
                                baseFragment = new ConsultantFragment();
                                break;



                        case  PPT :
                                if(VIDEO_PLAYER.equals(NATIVE)) {
                                        baseFragment = new PptLiveMediaPlayerFragment();

                                } else {
                                        baseFragment = new PptExoPlayerFragment();
                                }
                                break;


                        case  EXCEL :
                                baseFragment = new ExcelFragment();
                                break;


                        case  PDF :
                                baseFragment = new PdfFragment();
                                break;



                        case  AUDIO :
                                baseFragment = new AudioFragment();
                                break;


                        case  RADIO :
                                baseFragment = new RadioFragment();
                                break;


                        default: /*Do nothing*/
                                // TODO : Add default_page fragment
                }

                return baseFragment;
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
                throw new CloneNotSupportedException("Cannot clone singleton class use getInstance() instead");
        }
}
