package com.plustv.prosignsdk.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.StrictMode;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.plustv.prosignsdk.ImageConverter;
import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.common.FileUtility;
import com.plustv.prosignsdk.holder.DataFlipperHolder;
import com.plustv.prosignsdk.models.Asset;
import com.plustv.prosignsdk.models.CustomDepartment;
import com.plustv.prosignsdk.models.CustomDoctor;
import com.plustv.prosignsdk.models.CustomDoctorList;
import com.plustv.prosignsdk.models.Properties;

import java.io.File;
import java.util.List;


public class DirectoryAdapter extends BaseAdapter {
    private Context context;
    private List<CustomDoctorList> customDoctorLists;
    private Properties properties;
    private Asset asset;


    public DirectoryAdapter(Context context, List<CustomDoctorList> customDoctorLists, Properties properties, Asset asset) {
        this.context = context;
        this.customDoctorLists = customDoctorLists;
        this.properties = properties;
        this.asset = asset;
    }

    @Override
    public int getCount() {
        return customDoctorLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.directory_item,null);

        DataFlipperHolder holder = new DataFlipperHolder();
        CustomDoctorList customDoctor = customDoctorLists.get(position);
        List<CustomDepartment> customDepartments = customDoctor.departments;



        // *****  asset set ****** //
        Typeface typefaceBold = Typeface.createFromAsset(context.getAssets(), "font/gotham_rounded_bold_21016.ttf");
        Typeface typefaceLight = Typeface.createFromAsset(context.getAssets(), "font/gotham_rounded_light_21020.ttf");

        String color = properties.color; //rest info
        String departmentNameColor = properties.departmentNameColor;  // departmentNameColor
        String departmentHeaderBackgroundColor = properties.departmentHeaderColor;  // departmentHeaderBackgroundColor
        String doctorNameColor = properties.doctorNameColor;  // doctorNameColor


        int departmentNameColorCode = Color.parseColor(departmentNameColor);
        int departmentHeaderBackgroundColorCode = Color.parseColor(departmentHeaderBackgroundColor);
        int doctorNameColorCode = Color.parseColor(doctorNameColor);

        int departmentNameSize = properties.departmentNameSize;  //sp  departmentNameSize
        int doctorNameSize = properties.doctorNameSize;  //sp
        int fontSize = properties.fontSize;  // sp for rest fontSize

        holder.lTopLayout = view.findViewById(R.id.lTopLayout);



/**
 * ******* for 2 departments
 */
    if(customDepartments.size()< 3  && customDepartments.size() > 1) {


            for (CustomDepartment department : customDepartments) {


                       LinearLayout lcustomTopLayout = new LinearLayout(this.context);
                       LinearLayout.LayoutParams paramSet = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                       lcustomTopLayout.setOrientation(LinearLayout.VERTICAL);
                       lcustomTopLayout.setLayoutParams(paramSet);


                       // add textview for department heading //
                       TextView tvDepartment = new TextView(this.context);
                       tvDepartment.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                               ViewGroup.LayoutParams.WRAP_CONTENT));
                       tvDepartment.setText(department.name.toUpperCase());
                       tvDepartment.setTextSize(TypedValue.COMPLEX_UNIT_SP, departmentNameSize);
                       tvDepartment.setTextColor(departmentNameColorCode);
                       tvDepartment.setTypeface(typefaceBold);
                       tvDepartment.setPadding(1,1,1,1);
                       tvDepartment.setBackgroundColor(departmentHeaderBackgroundColorCode);
                       tvDepartment.setGravity(Gravity.CENTER);
                       lcustomTopLayout.addView(tvDepartment);
                       holder.lTopLayout.addView(lcustomTopLayout);


                       for (CustomDoctor doct : department.doctorDetails) {


                           //lDoctLayout for all details //
                           LinearLayout lDoctLayout = new LinearLayout(this.context);
                           LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                           lDoctLayout.setOrientation(LinearLayout.VERTICAL);
                           lDoctLayout.setLayoutParams(param);
                           //lDoctLayout.setBackgroundResource(R.drawable.border);


                           //lDoctDetailsLayout for doctor details //
                           LinearLayout lDoctDetailsLayout = new LinearLayout(this.context);
                           LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                           lDoctDetailsLayout.setOrientation(LinearLayout.HORIZONTAL);
                           lDoctDetailsLayout.setPadding(20, 25, 20, 25);
                           lDoctDetailsLayout.setLayoutParams(params);



                           // lImgLayout for doctor image view //
                           LinearLayout lImgLayout = new LinearLayout(this.context);
                           lImgLayout.setOrientation(LinearLayout.HORIZONTAL);
                           lImgLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                           // lLayout.setPadding(20, 20, 20, 20);



                           ImageView imageView = new ImageView(this.context);
                           imageView.setLayoutParams(new android.view.ViewGroup.LayoutParams(240,240));


                           String imageSrc = doct.src;
                           String filename = imageSrc.substring(imageSrc.lastIndexOf("/")+1);



                           File file = FileUtility.getFile(filename);
                           Log.e("file-2","file-2  "+ file.getName());
                           Log.e("filename-2","filename-2  "+ filename);




                           StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                           StrictMode.setThreadPolicy(policy);

                          // Bitmap myImage = getBitmapFromURL(imageSrc);
                           Bitmap myImage;
                           myImage = BitmapFactory.decodeFile(file.getAbsolutePath());
                           Log.e("myImage-2","myImage-2  "+ myImage);

                           if(myImage == null) {
                               myImage = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);

                           }


                           if (file.exists() &&
                                       !FileUtility.isLockExists(file.getName())) {
                               Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(myImage, 1000);
                               imageView.setImageBitmap(circularBitmap);

                           }

                           else {

                           Bitmap bitmap = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);
                           Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 1000);
                           imageView.setImageBitmap(circularBitmap);

                           }






                           lImgLayout.addView(imageView);


                           lDoctDetailsLayout.addView(lImgLayout);


                           String name =  doct.name.toUpperCase();
                           String qualification = doct.qualification;
                           String designation = doct.designation;
                           String all =qualification + "\n" + designation;
                           // String all = name + "\n" + qualification + "\n" + designation;


                           // DoctorInfo Layout //
                           LinearLayout lDoctorInfoLayout = new LinearLayout(this.context);
                           lDoctorInfoLayout.setOrientation(LinearLayout.VERTICAL);
                           lDoctorInfoLayout.setGravity(Gravity.CENTER);
                           lDoctorInfoLayout.setPadding(60, 0, 0, 0);
                           lDoctorInfoLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


                           //Add Text view for doctor name //
                           TextView doctName = new TextView(this.context);
                           doctName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                   ViewGroup.LayoutParams.WRAP_CONTENT));
                           doctName.setText(name);
                           doctName.setTextSize(TypedValue.COMPLEX_UNIT_SP, doctorNameSize);
                           doctName.setTextColor(doctorNameColorCode); // hex color 0xAARRGGBB 0xffffffff
                           doctName.setTypeface(typefaceBold);
                           doctName.setGravity(Gravity.LEFT);
                           lDoctorInfoLayout.addView(doctName);

                           // Add textview
                           TextView doctDegisnation = new TextView(this.context);
                           doctDegisnation.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                   ViewGroup.LayoutParams.WRAP_CONTENT));
                           doctDegisnation.setText(doct.designation);
                           doctDegisnation.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                           doctDegisnation.setTextColor(Color.parseColor(color)); // hex color 0xAARRGGBB 0xffffffff
                           doctDegisnation.setTypeface(typefaceLight);
                           doctDegisnation.setGravity(Gravity.LEFT);
                           lDoctorInfoLayout.addView(doctDegisnation);


                           // Add textview
                           TextView doctQualification = new TextView(this.context);
                           doctQualification.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                   ViewGroup.LayoutParams.WRAP_CONTENT));
                           doctQualification.setText(doct.qualification);
                           doctQualification.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                           doctQualification.setTextColor(Color.parseColor(color)); // hex color 0xAARRGGBB 0xffffffff
                           doctQualification.setTypeface(typefaceLight);
                           doctQualification.setGravity(Gravity.LEFT);
                           lDoctorInfoLayout.addView(doctQualification);
                           lDoctDetailsLayout.addView(lDoctorInfoLayout);
                           lDoctLayout.addView(lDoctDetailsLayout);
                           lcustomTopLayout.addView(lDoctLayout);

                       }

            }
        }




/**
 * ******* for 1 department
 */

    else if(customDepartments.size() == 1) {
            for (CustomDepartment department : customDepartments) {
                // Log.e("dept", "-----dept-----" + department.name);
                //Log.e("size", "size--" + department.doctorDetails.size());


                if (department.doctorDetails.size() == 5) {

                    LinearLayout lcustomTopLayout = new LinearLayout(this.context);
                    LinearLayout.LayoutParams paramSet = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 10f);
                    lcustomTopLayout.setOrientation(LinearLayout.VERTICAL);
                    lcustomTopLayout.setLayoutParams(paramSet);


                    // add textview for department heading //
                    TextView tvDepartment = new TextView(this.context);
                    tvDepartment.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    tvDepartment.setText(department.name.toUpperCase());
                    tvDepartment.setTextSize(TypedValue.COMPLEX_UNIT_SP, departmentNameSize);
                    tvDepartment.setTextColor(departmentNameColorCode); // hex color 0xAARRGGBB 0xffffffff
                    tvDepartment.setTypeface(typefaceBold);
                    tvDepartment.setPadding(1,1,1,1);
                    tvDepartment.setBackgroundColor(departmentHeaderBackgroundColorCode);
                    tvDepartment.setGravity(Gravity.CENTER);
                    lcustomTopLayout.addView(tvDepartment);
                    holder.lTopLayout.addView(lcustomTopLayout);


                    for (CustomDoctor doct : department.doctorDetails) {


                        //lDoctLayout for all details //
                        LinearLayout lDoctLayout = new LinearLayout(this.context);
                        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f);
                        lDoctLayout.setOrientation(LinearLayout.VERTICAL);
                        lDoctLayout.setLayoutParams(param);
                        //lDoctLayout.setBackgroundResource(R.drawable.border);


                        //lDoctDetailsLayout for doctor details //
                        LinearLayout lDoctDetailsLayout = new LinearLayout(this.context);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        lDoctDetailsLayout.setOrientation(LinearLayout.HORIZONTAL);
                        lDoctDetailsLayout.setPadding(20, 25, 20, 25);
                        lDoctDetailsLayout.setLayoutParams(params);


                        // lImgLayout for doctor image view //
                        LinearLayout lImgLayout = new LinearLayout(this.context);
                        lImgLayout.setOrientation(LinearLayout.HORIZONTAL);
                        lImgLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        // lLayout.setPadding(20, 20, 20, 20);

                        ImageView imageView = new ImageView(this.context);
                        imageView.setLayoutParams(new android.view.ViewGroup.LayoutParams(260,260));



                        String imageSrc = doct.src;
                        String filename = imageSrc.substring(imageSrc.lastIndexOf("/")+1);



                        File file = FileUtility.getFile(filename);



                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);

                        Bitmap myImage;

                         myImage = BitmapFactory.decodeFile(file.getAbsolutePath());
                        Log.e("myImage-1","myImage -1 "+ myImage);

                        if(myImage == null) {
                            myImage = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);

                        }

                        if (file.exists() &&
                                !FileUtility.isLockExists(file.getName())) {
                            Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(myImage, 1000);
                            imageView.setImageBitmap(circularBitmap);

                        }

                        else {

                            Bitmap bitmap = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);
                            Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 1000);
                            imageView.setImageBitmap(circularBitmap);

                        }




                        lImgLayout.addView(imageView);
                        lDoctDetailsLayout.addView(lImgLayout);


                        String name = doct.name.toUpperCase();
                        String qualification = doct.qualification;
                        String designation = doct.designation;
                        String all = qualification + "\n" + designation;


                        // DoctorInfo Layout //
                        LinearLayout lDoctorInfoLayout = new LinearLayout(this.context);
                        lDoctorInfoLayout.setOrientation(LinearLayout.VERTICAL);
                        lDoctorInfoLayout.setGravity(Gravity.CENTER);
                        lDoctorInfoLayout.setPadding(60, 0, 0, 0);
                        lDoctorInfoLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


                        //Add Text view for doctor name //
                        TextView doctName = new TextView(this.context);
                        doctName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                        doctName.setText(name);
                        doctName.setTextSize(TypedValue.COMPLEX_UNIT_SP, doctorNameSize);
                        doctName.setTextColor(doctorNameColorCode); // hex color 0xAARRGGBB 0xffffffff
                        doctName.setTypeface(typefaceBold);
                        doctName.setGravity(Gravity.LEFT);
                        lDoctorInfoLayout.addView(doctName);


                        // Add textview
                        TextView doctDegisnation = new TextView(this.context);
                        doctDegisnation.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                        doctDegisnation.setText(doct.designation);
                        doctDegisnation.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                        doctDegisnation.setTextColor(Color.parseColor(color)); // hex color 0xAARRGGBB 0xffffffff
                        doctDegisnation.setTypeface(typefaceLight);
                        doctDegisnation.setGravity(Gravity.LEFT);
                        lDoctorInfoLayout.addView(doctDegisnation);


                        // Add textview
                        TextView doctQualification = new TextView(this.context);
                        doctQualification.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                        doctQualification.setText(doct.qualification);
                        doctQualification.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                        doctQualification.setTextColor(Color.parseColor(color)); // hex color 0xAARRGGBB 0xffffffff
                        doctQualification.setTypeface(typefaceLight);
                        doctQualification.setGravity(Gravity.LEFT);
                        lDoctorInfoLayout.addView(doctQualification);
                        lDoctDetailsLayout.addView(lDoctorInfoLayout);
                        lDoctLayout.addView(lDoctDetailsLayout);
                        lcustomTopLayout.addView(lDoctLayout);

                    }
                } else {


                    LinearLayout lcustomTopLayout = new LinearLayout(this.context);
                    LinearLayout.LayoutParams paramSet = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lcustomTopLayout.setOrientation(LinearLayout.VERTICAL);
                    lcustomTopLayout.setLayoutParams(paramSet);


                    // add textview for department heading //
                    TextView tvDepartment = new TextView(this.context);
                    tvDepartment.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    tvDepartment.setText(department.name.toUpperCase());
                    tvDepartment.setTextSize(TypedValue.COMPLEX_UNIT_SP, departmentNameSize);
                    tvDepartment.setTextColor(departmentNameColorCode); // hex color 0xAARRGGBB 0xffffffff
                    tvDepartment.setTypeface(typefaceBold);
                    tvDepartment.setPadding(1,1,1,1);
                    tvDepartment.setBackgroundColor(departmentHeaderBackgroundColorCode);
                    tvDepartment.setGravity(Gravity.CENTER);
                    lcustomTopLayout.addView(tvDepartment);
                    holder.lTopLayout.addView(lcustomTopLayout);


                    for (CustomDoctor doct : department.doctorDetails) {


                        //lDoctLayout for all details //
                        LinearLayout lDoctLayout = new LinearLayout(this.context);
                        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT

                        );
                        lDoctLayout.setOrientation(LinearLayout.VERTICAL);
                        lDoctLayout.setLayoutParams(param);
                        //lDoctLayout.setBackgroundResource(R.drawable.border);


                        //lDoctDetailsLayout for doctor details //
                        LinearLayout lDoctDetailsLayout = new LinearLayout(this.context);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        lDoctDetailsLayout.setOrientation(LinearLayout.HORIZONTAL);
                        lDoctDetailsLayout.setPadding(20, 25, 20, 20);
                        lDoctDetailsLayout.setLayoutParams(params);


                        // lImgLayout for doctor image view //
                        LinearLayout lImgLayout = new LinearLayout(this.context);
                        lImgLayout.setOrientation(LinearLayout.HORIZONTAL);
                        lImgLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                        ImageView imageView = new ImageView(this.context);
                        imageView.setLayoutParams(new android.view.ViewGroup.LayoutParams(260,260));

                        String imageSrc = doct.src;


                        String filename = imageSrc.substring(imageSrc.lastIndexOf("/")+1);



                        File file = FileUtility.getFile(filename);



                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);

                        Bitmap myImage ;
                         myImage = BitmapFactory.decodeFile(file.getAbsolutePath());
                        Log.e("myImage-1-else","myImage -1-else "+ myImage);

                        if(myImage == null) {
                            myImage = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);
                        }



                        if (file.exists() &&
                                !FileUtility.isLockExists(file.getName())) {
                            Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(myImage, 1000);
                            imageView.setImageBitmap(circularBitmap);

                        }

                        else {

                            Bitmap bitmap = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);
                            Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 1000);
                            imageView.setImageBitmap(circularBitmap);

                        }





                        lImgLayout.addView(imageView);


                        lDoctDetailsLayout.addView(lImgLayout);


                        String name = doct.name.toUpperCase();
                        String qualification = doct.qualification;
                        String designation = doct.designation;
                        String all = qualification + "\n" + designation;


                        // DoctorInfo Layout //
                        LinearLayout lDoctorInfoLayout = new LinearLayout(this.context);
                        lDoctorInfoLayout.setOrientation(LinearLayout.VERTICAL);
                        lDoctorInfoLayout.setGravity(Gravity.CENTER);
                        lDoctorInfoLayout.setPadding(60, 0, 0, 0);
                        lDoctorInfoLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


                        //Add Text view for doctor name //
                        TextView doctName = new TextView(this.context);
                        doctName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                        doctName.setText(name);
                        doctName.setTextSize(TypedValue.COMPLEX_UNIT_SP, doctorNameSize);
                        doctName.setTextColor(doctorNameColorCode); // hex color 0xAARRGGBB 0xffffffff
                        doctName.setTypeface(typefaceBold);
                        doctName.setGravity(Gravity.LEFT);
                        lDoctorInfoLayout.addView(doctName);


                        // Add textview
                        TextView doctDetails = new TextView(this.context);
                        doctDetails.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                        doctDetails.setText(all);
                        doctDetails.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                        doctDetails.setTextColor(Color.parseColor(color)); // hex color 0xAARRGGBB 0xffffffff
                        doctDetails.setTypeface(typefaceLight);
                        doctDetails.setGravity(Gravity.LEFT);
                        lDoctorInfoLayout.addView(doctDetails);
                        lDoctDetailsLayout.addView(lDoctorInfoLayout);
                        lDoctLayout.addView(lDoctDetailsLayout);
                        lcustomTopLayout.addView(lDoctLayout);


                    }

                }
            }
        }




/**
 * ********** for 3 departments
 */
    else if(customDepartments.size()== 3 ) {


            for (CustomDepartment department : customDepartments) {


                LinearLayout lcustomTopLayout = new LinearLayout(this.context);
                LinearLayout.LayoutParams paramSet = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lcustomTopLayout.setOrientation(LinearLayout.VERTICAL);
                lcustomTopLayout.setLayoutParams(paramSet);


                // add textview for department heading //
                TextView tvDepartment = new TextView(this.context);
                tvDepartment.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                tvDepartment.setText(department.name.toUpperCase());
                tvDepartment.setTextSize(TypedValue.COMPLEX_UNIT_SP, departmentNameSize);
                tvDepartment.setTextColor(departmentNameColorCode);
                tvDepartment.setTypeface(typefaceBold);
                tvDepartment.setPadding(1,1,1,1);
                tvDepartment.setBackgroundColor(departmentHeaderBackgroundColorCode);
                tvDepartment.setGravity(Gravity.CENTER);
                lcustomTopLayout.addView(tvDepartment);
                holder.lTopLayout.addView(lcustomTopLayout);


                for (CustomDoctor doct : department.doctorDetails) {


                    //lDoctLayout for all details //
                    LinearLayout lDoctLayout = new LinearLayout(this.context);
                    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lDoctLayout.setOrientation(LinearLayout.VERTICAL);
                    lDoctLayout.setLayoutParams(param);
                    //lDoctLayout.setBackgroundResource(R.drawable.border);


                    //lDoctDetailsLayout for doctor details //
                    LinearLayout lDoctDetailsLayout = new LinearLayout(this.context);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lDoctDetailsLayout.setOrientation(LinearLayout.HORIZONTAL);
                    lDoctDetailsLayout.setPadding(20, 25, 20, 25);
                    lDoctDetailsLayout.setLayoutParams(params);


                    // lImgLayout for doctor image view //
                    LinearLayout lImgLayout = new LinearLayout(this.context);
                    lImgLayout.setOrientation(LinearLayout.HORIZONTAL);
                    lImgLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                    ImageView imageView = new ImageView(this.context);
                    imageView.setLayoutParams(new android.view.ViewGroup.LayoutParams(230,230));

                    CircularImageView image = new CircularImageView(this.context);

                    String imageSrc = doct.src;


                    String filename = imageSrc.substring(imageSrc.lastIndexOf("/")+1);



                    File file = FileUtility.getFile(filename);



                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);

                    Bitmap myImage;
                     myImage = BitmapFactory.decodeFile(file.getAbsolutePath());
                    Log.e("myImage-3","myImage-3  "+ myImage);

                    if(myImage == null) {
                        myImage = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);

                    }

                    if (file.exists() &&
                            !FileUtility.isLockExists(file.getName())) {
                        Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(myImage, 1000);
                        imageView.setImageBitmap(circularBitmap);

                    }

                    else {

                        Bitmap bitmap = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);
                        Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 1000);
                        imageView.setImageBitmap(circularBitmap);

                    }






                    lImgLayout.addView(imageView);


                    lDoctDetailsLayout.addView(lImgLayout);


                    String name = doct.name.toUpperCase();
                    String qualification = doct.qualification;
                    String designation = doct.designation;
                    String all =qualification + "\n" + designation;


                    // DoctorInfo Layout //
                    LinearLayout lDoctorInfoLayout = new LinearLayout(this.context);
                    lDoctorInfoLayout.setOrientation(LinearLayout.VERTICAL);
                    lDoctorInfoLayout.setGravity(Gravity.CENTER);
                    lDoctorInfoLayout.setPadding(60, 0, 0, 0);
                    lDoctorInfoLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


                    //Add Text view for doctor name //
                    TextView doctName = new TextView(this.context);
                    doctName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    doctName.setText(name);
                    doctName.setTextSize(TypedValue.COMPLEX_UNIT_SP, doctorNameSize);
                    doctName.setTextColor(doctorNameColorCode); // hex color 0xAARRGGBB 0xffffffff
                    doctName.setTypeface(typefaceBold);
                    doctName.setGravity(Gravity.LEFT);
                    lDoctorInfoLayout.addView(doctName);

                    // Add textview
                    TextView doctDegisnation = new TextView(this.context);
                    doctDegisnation.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    doctDegisnation.setText(doct.designation);
                    doctDegisnation.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                    doctDegisnation.setTextColor(Color.parseColor(color)); // hex color 0xAARRGGBB 0xffffffff
                    doctDegisnation.setTypeface(typefaceLight);
                    doctDegisnation.setGravity(Gravity.LEFT);
                    lDoctorInfoLayout.addView(doctDegisnation);


                    // Add textview
                    TextView doctQualification = new TextView(this.context);
                    doctQualification.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    doctQualification.setText(doct.qualification);
                    doctQualification.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                    doctQualification.setTextColor(Color.parseColor(color)); // hex color 0xAARRGGBB 0xffffffff
                    doctQualification.setTypeface(typefaceLight);
                    doctQualification.setGravity(Gravity.LEFT);
                    lDoctorInfoLayout.addView(doctQualification);
                    lDoctDetailsLayout.addView(lDoctorInfoLayout);
                    lDoctLayout.addView(lDoctDetailsLayout);
                    lcustomTopLayout.addView(lDoctLayout);

                }

            }
        }




/**
 * ******* for 4 departments
 */

    else if(customDepartments.size()== 4 ) {


            for (CustomDepartment department : customDepartments) {


                LinearLayout lcustomTopLayout = new LinearLayout(this.context);
                LinearLayout.LayoutParams paramSet = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lcustomTopLayout.setOrientation(LinearLayout.VERTICAL);
                lcustomTopLayout.setLayoutParams(paramSet);


                // add textview for department heading //
                TextView tvDepartment = new TextView(this.context);
                tvDepartment.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                tvDepartment.setText(department.name.toUpperCase());
                tvDepartment.setTextSize(TypedValue.COMPLEX_UNIT_SP, departmentNameSize);
                tvDepartment.setTextColor(departmentNameColorCode);
                tvDepartment.setTypeface(typefaceBold);
                tvDepartment.setPadding(1,1,1,1);
                tvDepartment.setBackgroundColor(departmentHeaderBackgroundColorCode);
                tvDepartment.setGravity(Gravity.CENTER);
                lcustomTopLayout.addView(tvDepartment);
                holder.lTopLayout.addView(lcustomTopLayout);


                for (CustomDoctor doct : department.doctorDetails) {


                    //lDoctLayout for all details //
                    LinearLayout lDoctLayout = new LinearLayout(this.context);
                    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lDoctLayout.setOrientation(LinearLayout.VERTICAL);
                    lDoctLayout.setLayoutParams(param);
                    //lDoctLayout.setBackgroundResource(R.drawable.border);


                    //lDoctDetailsLayout for doctor details //
                    LinearLayout lDoctDetailsLayout = new LinearLayout(this.context);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lDoctDetailsLayout.setOrientation(LinearLayout.HORIZONTAL);
                    lDoctDetailsLayout.setPadding(20, 25, 20, 25);
                    lDoctDetailsLayout.setLayoutParams(params);


                    // lImgLayout for doctor image view //
                    LinearLayout lImgLayout = new LinearLayout(this.context);
                    lImgLayout.setOrientation(LinearLayout.HORIZONTAL);
                    lImgLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    ImageView imageView = new ImageView(this.context);
                    imageView.setLayoutParams(new android.view.ViewGroup.LayoutParams(210,210));


                    CircularImageView image = new CircularImageView(this.context);

                    String imageSrc = doct.src;

                    String filename = imageSrc.substring(imageSrc.lastIndexOf("/")+1);



                    File file = FileUtility.getFile(filename);



                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);

                    Bitmap myImage;
                     myImage = BitmapFactory.decodeFile(file.getAbsolutePath());
                    Log.e("myImage-4","myImage-4  "+ myImage);

                    if(myImage == null) {
                        myImage = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);

                    }


                    if (file.exists() &&
                            !FileUtility.isLockExists(file.getName())) {
                        Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(myImage, 1000);
                        imageView.setImageBitmap(circularBitmap);

                    }

                    else {

                        Bitmap bitmap = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);
                        Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 1000);
                        imageView.setImageBitmap(circularBitmap);

                    }





                    lImgLayout.addView(imageView);
                    lDoctDetailsLayout.addView(lImgLayout);


                    String name = doct.name.toUpperCase();
                    String qualification = doct.qualification;
                    String designation = doct.designation;
                    String all =qualification + "\n" + designation;


                    // DoctorInfo Layout //
                    LinearLayout lDoctorInfoLayout = new LinearLayout(this.context);
                    lDoctorInfoLayout.setOrientation(LinearLayout.VERTICAL);
                    lDoctorInfoLayout.setGravity(Gravity.CENTER);
                    lDoctorInfoLayout.setPadding(60, 0, 0, 0);
                    lDoctorInfoLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


                    //Add Text view for doctor name //
                    TextView doctName = new TextView(this.context);
                    doctName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    doctName.setText(name);
                    doctName.setTextSize(TypedValue.COMPLEX_UNIT_SP, doctorNameSize);
                    doctName.setTextColor(doctorNameColorCode); // hex color 0xAARRGGBB 0xffffffff
                    doctName.setTypeface(typefaceBold);
                    doctName.setGravity(Gravity.LEFT);
                    lDoctorInfoLayout.addView(doctName);

                    // Add textview
                    TextView doctDegisnation = new TextView(this.context);
                    doctDegisnation.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    doctDegisnation.setText(doct.designation);
                    doctDegisnation.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                    doctDegisnation.setTextColor(Color.parseColor(color)); // hex color 0xAARRGGBB 0xffffffff
                    doctDegisnation.setTypeface(typefaceLight);
                    doctDegisnation.setGravity(Gravity.LEFT);
                    lDoctorInfoLayout.addView(doctDegisnation);


                    // Add textview
                    TextView doctQualification = new TextView(this.context);
                    doctQualification.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    doctQualification.setText(doct.qualification);
                    doctQualification.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                    doctQualification.setTextColor(Color.parseColor(color)); // hex color 0xAARRGGBB 0xffffffff
                    doctQualification.setTypeface(typefaceLight);
                    doctQualification.setGravity(Gravity.LEFT);
                    lDoctorInfoLayout.addView(doctQualification);
                    lDoctDetailsLayout.addView(lDoctorInfoLayout);
                    lDoctLayout.addView(lDoctDetailsLayout);
                    lcustomTopLayout.addView(lDoctLayout);

                }

            }
        }




/**
 * ******* else part
 */
    else {

            for (CustomDepartment department : customDepartments) {

                LinearLayout lcustomTopLayout = new LinearLayout(this.context);
                LinearLayout.LayoutParams paramSet = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 10f);
                lcustomTopLayout.setOrientation(LinearLayout.VERTICAL);
                lcustomTopLayout.setLayoutParams(paramSet);


                // add textview for department heading //
                TextView tvDepartment = new TextView(this.context);
                tvDepartment.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                tvDepartment.setText(department.name.toUpperCase());
                tvDepartment.setTextSize(TypedValue.COMPLEX_UNIT_SP, departmentNameSize);
                tvDepartment.setTextColor(departmentNameColorCode); // hex color 0xAARRGGBB 0xffffffff
                tvDepartment.setTypeface(typefaceBold);
                tvDepartment.setPadding(1,1,1,1);
                tvDepartment.setBackgroundColor(departmentHeaderBackgroundColorCode);
                tvDepartment.setGravity(Gravity.CENTER);
                lcustomTopLayout.addView(tvDepartment);
                holder.lTopLayout.addView(lcustomTopLayout);


                for (CustomDoctor doct : department.doctorDetails) {
                     //Log.e("doct","doct--"+ doct.name);


                    //lDoctLayout for all details //
                    LinearLayout lDoctLayout = new LinearLayout(this.context);
                    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f);
                    lDoctLayout.setOrientation(LinearLayout.VERTICAL);
                    lDoctLayout.setLayoutParams(param);
                    lDoctLayout.getWeightSum();
                    //lDoctLayout.setBackgroundResource(R.drawable.border);


                    //lDoctDetailsLayout for doctor details //
                    LinearLayout lDoctDetailsLayout = new LinearLayout(this.context);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lDoctDetailsLayout.setOrientation(LinearLayout.HORIZONTAL);
                    lDoctDetailsLayout.setPadding(20, 25, 20, 20);
                    lDoctDetailsLayout.setLayoutParams(params);


                    // lImgLayout for doctor image view //
                    LinearLayout lImgLayout = new LinearLayout(this.context);
                    lImgLayout.setOrientation(LinearLayout.HORIZONTAL);
                    lImgLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    // lLayout.setPadding(20, 20, 20, 20);

                    ImageView imageView = new ImageView(this.context);
                    imageView.setLayoutParams(new android.view.ViewGroup.LayoutParams(200,200));

                    CircularImageView image = new CircularImageView(this.context);

                    String imageSrc = doct.src;


                    String filename = imageSrc.substring(imageSrc.lastIndexOf("/")+1);



                    File file = FileUtility.getFile(filename);



                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);

                    Bitmap myImage ;
                     myImage = BitmapFactory.decodeFile(file.getAbsolutePath());
                    Log.e("myImage-else","myImage-else "+ myImage);

                    if(myImage == null) {
                        myImage = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);
                    }


                    if (file.exists() &&
                            !FileUtility.isLockExists(file.getName())) {
                        Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(myImage, 1000);
                        imageView.setImageBitmap(circularBitmap);

                    }

                    else {

                        Bitmap bitmap = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.error_thumnail);
                        Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 1000);
                        imageView.setImageBitmap(circularBitmap);

                    }




                    lImgLayout.addView(imageView);
                    lDoctDetailsLayout.addView(lImgLayout);


                    String name = doct.name.toUpperCase();
                    String qualification = doct.qualification;
                    String designation = doct.designation;
                    String all =qualification + "\n" + designation;


                    // DoctorInfo Layout //
                    LinearLayout lDoctorInfoLayout = new LinearLayout(this.context);
                    lDoctorInfoLayout.setOrientation(LinearLayout.VERTICAL);
                    lDoctorInfoLayout.setGravity(Gravity.CENTER);
                    lDoctorInfoLayout.setPadding(60, 0, 0, 0);
                    lDoctorInfoLayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


                    //Add Text view for doctor name //
                    TextView doctName = new TextView(this.context);
                    doctName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    doctName.setText(name);
                    doctName.setTextSize(TypedValue.COMPLEX_UNIT_SP, doctorNameSize);
                    doctName.setTextColor(doctorNameColorCode); // hex color 0xAARRGGBB 0xffffffff
                    doctName.setTypeface(typefaceBold);
                    doctName.setGravity(Gravity.LEFT);
                    lDoctorInfoLayout.addView(doctName);


                    // Add textview
                    TextView doctDetails = new TextView(this.context);
                    doctDetails.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    doctDetails.setText(all);
                    doctDetails.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                    doctDetails.setTextColor(Color.parseColor(color)); // hex color 0xAARRGGBB 0xffffffff
                    doctDetails.setTypeface(typefaceLight);
                    doctDetails.setGravity(Gravity.LEFT);
                    lDoctorInfoLayout.addView(doctDetails);
                    lDoctDetailsLayout.addView(lDoctorInfoLayout);
                    lDoctLayout.addView(lDoctDetailsLayout);
                    lcustomTopLayout.addView(lDoctLayout);

                }
            }
        }


        return view;
    }

}

