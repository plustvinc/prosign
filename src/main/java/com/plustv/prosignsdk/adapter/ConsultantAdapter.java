package com.plustv.prosignsdk.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plustv.prosignsdk.R;
import com.plustv.prosignsdk.holder.ConsultantDataHolder;
import com.plustv.prosignsdk.models.CustomDoctorList;

import java.util.List;

public class ConsultantAdapter extends RecyclerView.Adapter<ConsultantDataHolder> {
Context context;
List<CustomDoctorList> customList ;



public ConsultantAdapter(Context context, List<CustomDoctorList> customList)
{
	super();
	this.context = context;
	this.customList = customList;
}



@Override
public ConsultantDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
	View v = LayoutInflater.from(parent.getContext())
		.inflate(R.layout.consultant_directory_item,parent,false);
	ConsultantDataHolder  dataHolder = new ConsultantDataHolder(v);
	return dataHolder;

}

@Override
public void onBindViewHolder(ConsultantDataHolder holder, int position) {

}

@Override
public int getItemCount() {
	return 1;
}
}

