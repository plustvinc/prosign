package com.plustv.prosignsdk;

import com.plustv.prosignsdk.common.FileUtility;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rachit Solanki on 17/8/18.
 */


@RunWith(RobolectricTestRunner.class)
public class FileUtilityTest {

@Test
public void decodeFilePathTestFULL() {
        assertEquals("test.png",
                FileUtility.decodeFileName("http://someurl:9000/path/nested/test.png"));

}
@Test
public void decodeFilePathTestNoURL() {
        assertEquals("test.png",
                FileUtility.decodeFileName("/path/nested/test.png"));

}
@Test
public void decodeFilePathTestNONE() {
        assertEquals("test.png",
                FileUtility.decodeFileName("test.png"));

}
@Test
public void decodeFilePathTestNULL() {
        assertEquals("",
                FileUtility.decodeFileName(null));

}
@Test
public void decodeFilePathTestEMPTY() {
        assertEquals(null,
                FileUtility.decodeFileName(""));

}


}
