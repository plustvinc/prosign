package com.plustv.prosignsdk;

import com.plustv.prosignsdk.common.BaseFragment;
import com.plustv.prosignsdk.common.FragmentPoolManager;
import com.plustv.prosignsdk.fragments.ClockFragment;
import com.plustv.prosignsdk.fragments.ExoPlayerFragment;
import com.plustv.prosignsdk.fragments.ImageFragment;
import com.plustv.prosignsdk.fragments.RssFragment;
import com.plustv.prosignsdk.fragments.ScrollTextFragment;
import com.plustv.prosignsdk.fragments.WebViewFragment;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Rachit Solanki on 17/8/18.
 */

@RunWith(RobolectricTestRunner.class)
public class FragmentPoolTest {

@Test
public void checkPoolInstanceIsSingleton() {
        FragmentPoolManager fragmentPoolManager = FragmentPoolManager.getInstance();
        FragmentPoolManager fragmentPoolManagerSecond = FragmentPoolManager.getInstance();
        assertEquals(fragmentPoolManager.hashCode(),
                        fragmentPoolManagerSecond.hashCode());

}

@Test
public void checkPoolInstanceIsImageFragment() {
        BaseFragment imgFrag = FragmentPoolManager.getInstance()
                                        .getFragmentFor(FragmentPoolManager.IMAGE_FRAGMENT);
        assertThat(imgFrag,instanceOf(ImageFragment.class));

}

@Test
public void checkPoolInstanceIsGIFFragment() {
        BaseFragment imgFrag = FragmentPoolManager.getInstance()
                                        .getFragmentFor(FragmentPoolManager.GIF_VIEW_FRAGMENT);
        assertThat(imgFrag,instanceOf(ImageFragment.class));

}

@Test
public void checkPoolInstanceIsVideoFragment() {
        BaseFragment imgFrag = FragmentPoolManager.getInstance()
                                        .getFragmentFor(FragmentPoolManager.VIDEO_FRAGMENT);
        assertThat(imgFrag,instanceOf(ExoPlayerFragment.class));

}

@Test
public void checkPoolInstanceIsScrollFragment() {
        BaseFragment imgFrag = FragmentPoolManager.getInstance()
                                        .getFragmentFor(FragmentPoolManager.SCROLL_TEXT_FRAGMENT);
        assertThat(imgFrag,instanceOf(ScrollTextFragment.class));

}

@Test
public void checkPoolInstanceIsRSSFragment() {
        BaseFragment imgFrag = FragmentPoolManager.getInstance()
                                        .getFragmentFor(FragmentPoolManager.RSS_FRAGMENT);
        assertThat(imgFrag,instanceOf(RssFragment.class));

}

@Test
public void checkPoolInstanceIsWebFragment() {
        BaseFragment imgFrag = FragmentPoolManager.getInstance()
                                        .getFragmentFor(FragmentPoolManager.WEB_VIEW_FRAGMENT);
        assertThat(imgFrag,instanceOf(WebViewFragment.class));

}

@Test
public void checkPoolInstanceIsClockFragment() {
        BaseFragment imgFrag = FragmentPoolManager.getInstance()
                                        .getFragmentFor(FragmentPoolManager.CLOCK_FRAGMENT);
        assertThat(imgFrag,instanceOf(ClockFragment.class));

}




}
