package com.plustv.prosignsdk;

import com.plustv.prosignsdk.common.RssParser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Rachit Solanki on 17/8/18.
 */
@RunWith(RobolectricTestRunner.class)
public class RssParserTest {

private String HEADER ="<rss xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:content=\"http://purl.org/rss/1.0/modules/content/\" xmlns:atom=\"http://www.w3.org/2005/Atom\" version=\"2.0\" xmlns:media=\"http://search.yahoo.com/mrss/\">\n <channel>";

private String FOOTER ="</channel></rss>";

private InputStream getInputStreamTitleAndBody() {
        String stringBuilder = HEADER + "<item>" +
                "<title><![CDATA[TitleTest]]></title>" +
                "<description><![CDATA[DescriptionTest]]></description>" +
                "<link><![CDATA[LinkTest]]></link>" +
                "</item>" +
                FOOTER;
        return new ByteArrayInputStream(stringBuilder.getBytes() );
}

private InputStream getInputStreamTitleOnly() throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder(HEADER);
        /*Content*/
        stringBuilder.append("<item>");
        stringBuilder.append("<title><![CDATA[TitleTest]]></title>");
        stringBuilder.append("<item>");
        /*Content*/
        stringBuilder.append(FOOTER);

        InputStream inputStream = new ByteArrayInputStream(stringBuilder.toString().getBytes("UTF-8") );
        return inputStream;
}

private InputStream getInputStreamBodyOnly() throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder(HEADER);
        /*Content*/
        stringBuilder.append("<item>");
        stringBuilder.append("<description><![CDATA[DescriptionTest]]></description>");
        stringBuilder.append("<item>");
        /*Content*/
        stringBuilder.append(FOOTER);

        InputStream inputStream = new ByteArrayInputStream(stringBuilder.toString().getBytes("UTF-8") );
        return inputStream;
}

private InputStream getInputStreamNone() throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder(HEADER);
        /*Content*/
        /*No content*/
        /*Content*/
        stringBuilder.append(FOOTER);

        InputStream inputStream = new ByteArrayInputStream(stringBuilder.toString().getBytes("UTF-8") );
        return inputStream;
}



@Test()
public void testRssParser() {

        try {
               assertEquals(" TitleTest ||",RssParser.rssFeed(getInputStreamTitleAndBody()) );;
        } catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
        }

}

@Test()
public void testRssParserTitleOnly() {

        try {
               RssParser.rssFeed(getInputStreamTitleOnly());
        } catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
        }

}

@Test()
public void testRssParserBodyOnly() {

        try {
               RssParser.rssFeed(getInputStreamBodyOnly());
        } catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
        }

}

@Test()
public void testRssParserNone() {

        try {
                assertEquals("",RssParser.rssFeed(getInputStreamNone()));
        } catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
        }

}


}
